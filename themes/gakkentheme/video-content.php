<?php
    /*Template Name : Single Content */
    $slug       = urldecode($wp_query->query_vars['video']);
    $content    = json_decode(do_shortcode('[gkvault-contents-single slug=' . $slug . ']'));
    $account    = json_decode( do_shortcode('[gkaccount-login-status]') );
    $date_today = date('Y-m-d H:i:s');
    $vaulturl   = do_shortcode('[gkvault-get-base-url]');
    $subscribe   = false;

    if ($account->status == 'authenticated' && $account->subscription) {
        $subscribe = $account->subscription->expired_at > $date_today;
    }

    function get_signed_player_w_video($videokey) {
        $playerkey = 'jHONf2kl';
        $path = "players/".$videokey."-".$playerkey.".js";
        $expires = round((time()+3600)/300)*300;
        $secret = "n0HwsH0j1tCSdkFAvpSTvxcx";
        $signature = md5($path.':'.$expires.':'.$secret);
        $url = 'http://content.jwplatform.com/'.$path.'?exp='.$expires.'&sig='.$signature;
        return $url;
    };

    function get_signed_url($videokey, $res = null) {
        // $path = "videos/" . $videokey;
        $path = "manifests/" . $videokey;
        // $path .= $res != null ? '-' . $res . '.mp4' : '.mp4';
        $path .= '.m3u8';
        $expires = round((time()+3600)/300)*300;
        $secret = "n0HwsH0j1tCSdkFAvpSTvxcx";
        $signature = md5($path.':'.$expires.':'.$secret);
        $url = '//content.jwplatform.com/'.$path.'?sig=' . $signature . '&exp=' . $expires;
        return $url;
    };

    function get_signed_player() {
        $playerkey = 'jHONf2kl';
        $path = "libraries/".$playerkey.".js";
        $expires = round((time()+3600)/300)*300;
        $secret = "n0HwsH0j1tCSdkFAvpSTvxcx";
        $signature = md5($path.':'.$expires.':'.$secret);
        $url = 'http://content.jwplatform.com/'.$path.'?exp='.$expires.'&sig='.$signature;
        return $url;
    };
?>

<!DOCTYPE html>
<html style="height:100%">
    <head>
        <title><?= 'Gakken Indonesia || ' . $content->title ?></title>

        <!-- <script src=" get_signed_player_w_video($content->url_jw)"></script> -->
        <script src="<?= get_signed_player() ?>" charset="utf-8"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <link href="<?= esc_url( get_template_directory_uri() ); ?>/stylesheets/screen-video.css" media="screen, projection" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= esc_url( get_template_directory_uri() ); ?>/stylesheets/font-awesome.css">
	    <!--[if IE]>
	       <link href="/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
	    <![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="<?= get_signed_player() ?>" charset="utf-8"></script>
    </head>
    <body style="height:100%">
        <?php if ($subscribe || $content->is_free): ?>
            <div class="gakkenvideo body-wrap" style="height:100%">
                <div class="body-container">
                    <div class="breadcrumb">
                        <span>
                            Topik
                        </span>
                        <i class="fa fa-angle-right"> </i>
                        <span> <a href="#" class="selected"><?= $content->title ?></a> </span>
                    </div>
                    <div class="video" id="player">
                    </div>
                    <!-- <div class="descriptions">
                        <h3> Chapter </h3>
                        <ul>
                            <li><button class="selected"> 1 : Background Check </button></li>
                            <li><button> 2 : Facts and Myth </button></li>
                            <li><button> 3 : Cases </button></li>
                            <li><button> 4 : Solutions </button></li>
                        </ul>

                        <article>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod dignissimos voluptatem, quis sequi velit voluptatibus enim in veritatis nesciunt explicabo eaque saepe quaerat voluptatum doloribus, aspernatur molestiae odio, culpa. Tenetur.</p>
                        </article>
                    </div> -->

                    <!-- <div class="nav"> <i class="fa fa-angle-left"> </i>  <span> <a href="#" class="selected"> Kembali ke topik </a> </span> </div> -->
                </div>
            </div>

            <script type="text/javascript">
            var player = jwplayer("player");

            player.setup({
                image: '//content.jwplatform.com/thumbs/<?= $content->url_jw ?>-480.jpg',
                file: "<?= get_signed_url($content->url_jw) ?>",
                autostart: true,

                // sources: [{
                //     file: " get_signed_url($content->url_jw, '4uRwA35p')" ,
                //     label: "180p"
                // },{
                //     file: " get_signed_url($content->url_jw, 'uihlrlP3')" ,
                //     label: "270p" ,
                //     default: true
                // },{
                //     file: " get_signed_url($content->url_jw, 'NI3G5jd5')" ,
                //     label: '406p' ,
                // },{
                //     file: " get_signed_url($content->url_jw, 'dEa30WFQ')" ,
                //     label: '720p HD'
                // }]
            });

            player.on('time', function(e) {
                localStorage.setItem('resumevideodata-<?= $slug ?>', Math.floor(e.position) + ':' + player.getDuration());
            });

            player.on('ready', function() {
                var cookieData = localStorage.getItem('resumevideodata-<?= $slug ?>');
                if (cookieData) {
                    var resumeAt = cookieData.split(':')[0],
                        videoDur = cookieData.split(':')[1];

                    if (parseInt(resumeAt) < parseInt(videoDur)) {
                        player.seek(resumeAt);
                        // console.log('Resuming at ' + resumeAt); //for demo purposes
                    } else if (cookieData && !(parseInt(resumeAt) < parseInt(videoDur))) {
                        // console.log('Video ended last time! Will skip resume behavior'); //for demo purposes
                    }
                } else {
                    // console.log('No video resume cookie detected. Refresh page.');
                }
            });
            </script>
        <?php else: ?>
            * Berlangganan <a href='<?= bloginfo('url') ?>/berlangganan'>disini</a>, untuk mengakses konten ini *
        <?php endif; ?>
    </body>
</html>
