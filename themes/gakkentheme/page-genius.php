<?php get_header() ?>

	<div class="jumbotron" style="background:url('<?php echo get_template_directory_uri() ?>/images/genius-cover.jpg') center center no-repeat;background-color:#1488CC;background-size:cover;background-blend-mode:multiply;padding-top:150px;padding-bottom:150px;color:#fff;">
		<div class="full-width">
			<div class="container-fluid">
				<h1 class="text-center">GENIUS</h1>
				<p class="text-center">Gakken Educational &amp; Customized Service</p>

				<div class="text-center">
					<a href="#content" style="color:#fff;">
						<b class="fa fa-angle-down fa-3x"></b>
					</a>
				</div>
			</div>
			
		</div>
	</div>

	<section class="full-width" id="content" style="padding-top:120px;padding-bottom:90px;">
		<div class="container-fluid jumptarget">
			<div class="row">
				<div class="col-lg-6">
					<h1 class="text-uppercase" style="font-size:32px;">
						Tailor-Made In Every Way. 
					</h1>
					<h1 class="text-muted" style="margin-bottom:15px;">
						Sesuai keinginan dan kebutuhan institusi dan bisnis Anda.
					</h1>
					<p style="margin-bottom:10px;">
						GENIUS hadir untuk memenuhi segala kebutuhan multimedia-based learning
						institusi dan bisnis Anda. Melalui layanan Genius, Gakken menyediakan
						sarana bagi institusi dan bisnis Anda untuk meningkatkan mutu kompetensi pelayanan
						dan sumber daya manusianya.
					</p>
					<p>
						Bekali manajemen, tenaga kesehatan, tenaga pengajar dan mahasiswa anda
						dengan Genius. Rancang materi pembelajaran berstandar tinggi bersama kami
						untuk dibuat khusus sesuai kebutuhan Anda!
					</p>
				</div>
				<div class="col-lg-6">
					<h3 class="text-uppercase" style="font-size:18px;margin-bottom:15px;">Gunakan GENIUS untuk:</h3>
					<div class="row">
						<div class="col-sm-4 text-center">
							<img src="<?php echo get_template_directory_uri() ?>/images/genius-icon-stat.png" style="width:75%;" />
							<p>Pelatihan persiapan akreditasi Rumah Sakit dan institusi pendidikan</p>
						</div>
						<div class="col-sm-4 text-center">
							<img src="<?php echo get_template_directory_uri() ?>/images/genius-icon-staff.png" style="width:75%;" />
							<p>Pelatihan staf keperawatan dan tenaga kesehatan lainnya</p>
						</div>
						<div class="col-sm-4 text-center">
							<img src="<?php echo get_template_directory_uri() ?>/images/genius-icon-edu.png" style="width:75%;" />
							<p>Perkuliahan di institusi pendidikan kedokteran dan program pendidikan kesehatan lainnya</p>
						</div>
					</div>
					
				</div>
			</div>

			<div class="text-center" style="margin-top:30px;">
				<a href="#benefits">
					<b class="fa fa-angle-down fa-3x"></b>
				</a>
			</div>
		</div>
	</section>

	<div style="background-color:#ebebeb;">

		<section class="full-width" id="benefits" style="padding-top:90px;padding-bottom:90px;">
			<div class="container-fluid jumptarget">
				<div class="row">
					<div class="col-lg-6 text-center">
						<img src="<?php echo get_template_directory_uri() ?>/images/genius-part-1.png" style="width:70%;" />
					</div>
					<div class="col-lg-6" style="padding-top:90px;">
						<h1 class="text-uppercase" style="font-size:32px;margin-bottom:15px;">Manfaat dengan Hasil yang Terukur.</h1>
						<p style="margin-bottom:10px;">
							Dengan Genius, tingkatkan kesempatan institusi Anda untuk mendapatkan hasil akreditasi yang
							membanggakan, tidak hanya dari segi pelayanan tapi juga dari segi mutu sumber daya manusianya.
						</p>
						<p style="margin-bottom:10px;">
							Bagi institusi pendidikan, asah kemampuan para staf tenaga pengajar Anda untuk menjadi pembimbing
							yang inovatif dengan menyusun dan membawakan materi pembelajaran yang komprehensif dan
							menarik dalam bentuk audiovisual dan tekstual bagi mahasiswanya.
						</p>
						<p style="margin-bottom:10px;">
							Bagi institusi kesehatan, gunakan Genius untuk melatih staf Anda dalam menguasai materi-materi pendukung
							kesuksesan manajemen Rumah Sakit.
						</p>
					</div>
				</div>

				<div class="text-center">
					<a href="#access">
						<b class="fa fa-angle-down fa-3x"></b>
					</a>
				</div>
			</div>
		</section>
	</div>

	<div>
		<section class="full-width" id="access" style="padding-top:90px;">
			<div class="container-fluid jumptarget">
				<div class="row">
					<div class="col-lg-6" style="padding-top:120px;">
						<h1 class="text-uppercase" style="font-size:32px;margin-bottom:15px;">Akses Dimana Saja, Kapan Saja.</h1>
						<p style="margin-bottom:10px;">
							Berikan kemudahan bagi institusi dan bisnis Anda dalam mengakses materi pembelajaran.
							Anda bisa memilih penyajian online melalui situs Gakken yang dapat diakses dimana saja, kapan
							saja, melalui perangkat apa saja.
						</p>
						<p style="margin-bottom:10px;">
							Selain itu, juga tersedia penyajian secara offline melalui media DVD yang diberikan langsung 
							kepada institusi dan bisnis Anda, dan bebas digunakan dalam lingkup yang Anda inginkan.
						</p>
					</div>
					<div class="col-lg-6 text-center">
						<img src="<?php echo get_template_directory_uri() ?>/images/genius-part-2.png" style="width:80%;" />
					</div>

				</div>
			</div>
		</section>
	</div>

	<div style="background-color:#1488CC;">
		<section class="full-width" style="padding-top:90px;padding-bottom:90px;margin-bottom:0;color:#fff;">
			<div class="container-fluid text-center">
				<h1 class="text-uppercase" style="font-weight:700;font-size:48px;">Interested?</h1>
				<h2 style="font-size:32px;">Hubungi tim kami sekarang juga!</h2>

				<h3 style="font-size:24px;margin-top:45px;">
					<b class="fa fa-fw fa-phone"></b> 0800-1-401652 (Toll Free) /
					<b class="fa fa-fw fa-envelope"></b> support@gakken-idn.co.id
				</h3>
			</div>
		</section>
	</div>

<?php get_footer() ?>