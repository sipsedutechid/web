/*
Theme Name: Gakken Website.
Theme URI: gakken-idn.id
Description: Gakken theme for the new website
Version: 1.0
Author: Aisyah Azalya
Author URI: syhzly.com
*/

@import "compass/reset";
@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic);
@import url(https://fonts.googleapis.com/css?family=Gentium+Book+Basic:400,400italic,700,700italic);

$gakken-red: #A41E22;

html {
  position: relative;
  min-height: 100%;
}

body{

 background: #f1f1f1;
 font-family: 'Gentium Book Basic', serif;
 word-wrap: break-word;
 border-sizing: border-box;
 margin-bottom: 268px;
}

nav, h1, h2, h3, h4, h5{
  font-family: 'Open Sans', sans-serif;
}

a{
  color: #555555;

  &:hover{
    color: $gakken-red;
    text-decoration: none;
  }
}

.fixed-nv{
  min-height: 60px;
  background: #fff;
  border-bottom: 4px solid #A41E22;
  position: fixed;
  width:100%;
  z-index: 9999;

  .container-fluid{
    padding: 10px;

        .logo {
          overflow:hidden;

            a{
              padding: 5px 15px;;

              img{
              width: 150px;
              height: auto;
            }
          }
        }
  }
}

.nvdown{
  webkit-animation: Down .5s 1 forwards;
  animation: Down .5s 1 forwards;
}

.nvup{
  -webkit-animation: Up .5s 1 forwards;
  animation: Up .5s 1 forwards;
}


@keyframes Down{
      0% {
        -webkit-transform: translateY(-100%);
        transform: translateY(-100%);
        visibility: visible;
        }

    100% {
        -webkit-transform: translateY(0);
        transform: translateY(0);
        }
}

@keyframes Up{
      0% {
        -webkit-transform: translateY(0);
        transform: translateY(0);
    }

    100% {
        visibility: hidden;
        -webkit-transform: translateY(-100%);
        transform: translateY(-100%);
    }
}

.dropdown-menu {
  padding-top: 0px;
  padding-botttom: 0px;

  & > li > a{

      padding: 5px 20px;
  }
}


.user-profile-wrap{

  .user-profile-wrap-btn{
    padding-top: 7px;
    border: none;
    background: none;

    .user-profile{
      width: 40px;
      height: 40px;
      border-radius: 25px;
    }
  }


  .user-tab{
    margin-top: 12px !important;
    margin-right: 5px;
    background: white;
    border-top-right-radius: 4px !important;
    border-top-left-radius: 4px !important;

    &:before{
      content: "\f0d8";
      position: absolute;
      margin-top: -24px;
      right: 10px;
      color: $gakken-red;
      font-size: 34px;
      background: transparent;
      font-family: 'FontAwesome';
    }
  }

}


.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .open > a{
  background: transparent;
  color: #BBBABA;
}

.highlight-btn{
  background-color: #EC1F27;
  color: #fff;
  border-color: #D20811;

  &:hover{
    background: #ebebeb;
    border-color: #D0D0D0;
  }
}

.full-width{
  margin: 0 auto;
  max-width: 100%;
  width: 1100px;
}

.static-nv-container{
  background: #fff;
  border-bottom: 10px solid $gakken-red;
}

.static-nv{
  border: none;
  background: transparent;
  margin-bottom: 0px;

  .navbar-brand{
    height: auto;
  }

  .navbar-nav{
    padding: 20px 0px;
  }

  .dropdown-menu{
    -webkit-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.175);
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.175);
    border: 0px !important;
  }

  .navbar-toggle{
    margin-top: 30px;
    // background: $gakken-red;

    // .icon-bar{
    //   background-color: #fff;
    // }
  }

}

.slider-container{
  padding: 20px 0px;
  overflow: hidden;

  .slider{
    height: 500px;
    background: #B3B3B3;
  }

  .slider-sidebar{
    padding-right: 0px !important;

    .small-sidebar{
      height: 150px;
      background: #B3B3B3;
      margin-bottom: 25px;
    }
  }
}

.main-container{

  .article-wrapper{
    padding-left: 0px;
    padding-right: 0px;
  }

  .article-container{
    padding-left: 0px;
    padding-right: 0px;
    padding-bottom: 10px;
    margin-bottom: 10px;
    position: relative;

    h1{
      color: $gakken-red;
      font-size: 120%;
      margin-top: 10px;
      margin-bottom: 10px;
      font-weight: 600;

    }

    .row{
      padding-top: 10px;

      .article-row{

        h3{
          font-weight: 600;
          font-size: 100%;
          line-height: 1.2em;
          padding: 5px 0px;
          text-align: justify;
        }

        .article-img{
          height: 200px;
          background: #B3B3B3;
        }
      }
    }


  }

  .article-container:not(last-child){
    border-bottom: 1px solid rgba(179, 179, 179, 0.35);
  }

  .single-article-container{
    background: #fff;
    margin-top: 50px;
    margin-bottom: 50px;
    padding: 20px;
    min-height: 1137px;

    section{
      overflow: hidden;
    }

    .page-header{
      margin: 10px 0 20px;
    }

    h1.title{

      font-weight: 700;
      line-height: 1.4;
      font-size: 180%;

      font-weight: 700;
      font-style: normal;
      // font-size: 36px;
      margin-left: -2.25px;
      line-height: 1.15;
      letter-spacing: -.02em;
    }

    h2.divider{
      color: $gakken-red;
      font-size: 130%;
      font-weight: 700;
      padding: 20px 10px;

      &:before{
        content: '';
        width: 20px;
        height: 30px;
        background: $gakken-red;
        display: block;
        float: left;
        /* margin-left: -20px; */
        position: absolute;
        left: 0;
        margin-top: -5px;
      }
    }

    .taught-by{
      background: #ebebeb;
      padding: 20px;
      margin: 20px 0px;
      position: relative;
      overflow: hidden;

      .taught-by-desc{
        line-height: 1.4;
        text-align: justify;
        width: 70%;
        float: left;

        h3,h4{
          font-weight: 600;
        }

        h3{
          padding-bottom: 10px;
        }
      }

      .taught-by-pic{
        padding: 15px;
        width: 30%;
        float: left;
      }

      .taught-by-pic-container{
        margin-top: 50px;
        width: 100%;
        min-height: 200px;
        border-radius: 100px;
      }
    }

    .sub-desc{
      padding-bottom: 20px;
      color: rgba(0,0,0,0.6);
    }

    article{

      img{
        max-width: 100%;
        padding-bottom: 10px;
      }

      p{
        padding: 5px 0px;
        line-height: 1.2;
        font-style: normal;
        font-size: 18px;
        line-height: 1.58;
        letter-spacing: -.003em;
      }
    }


  }

  .two-tab{

    .readmore{
      right: 10px;
    }

    .list-group{
      margin-top: 20px;

      a{
        padding: 20px;
        font-family: 'Open Sans', sans-serif;
        font-size: 110%;

        span.description{
          font-size: 85% !important;
          margin: 5px 0px;
        }
      }

    }
  }

  .login-wrapper{
    padding: 10px;
    background: #fff;
    margin: 20px 0px;
    position: relative;
    overflow: hidden;

    .login-form{
      margin: 0 auto;
      float: none;
      padding: 10px;

      h3,h4{
        text-align: center;
        padding: 5px 0px;
      }

      h4{
        font-size: 80%;
      }

      a{
        color: $gakken-red;
      }

      label{
        padding: 5px 0px;
      }

      .login-container{
        margin: 20px 0px;
        font-family: 'Open Sans', sans-serif;

        button{
          text-align: center;
          color: #FFF;
          background-color: #EC1F27;
          border-color: #EC1F27;

          &:hover{
            background: rgba(236, 31, 39, 0.54);
            border: 1px solid rgba(236, 31, 39, 0.54);
            color: #333;
          }
        }
      }
    }
  }
}

.readmore{
  font-size: 12px;
  right: 0;
  padding: 5px;
  position: absolute;
  font-weight: normal;

  a{
    color: #000;

    &:hover{
      color: $gakken-red;
    }
  }
}

.category-highlight{
  font-family: 'Open Sans', sans-serif;
  background-color: #A41E22;
  color: #fff;
  font-style: italic;
  padding: 3px;
  margin-top: 5px;
  margin-bottom: 5px;
  /* width: 100%; */
  right: 15px;
  text-align: right;
  position: absolute;
}

footer{
  position: absolute;
    bottom: 0;
    width: 100%;
    min-height: 268px;
}

section#footer{
  background: rgba(93, 93, 93, 0.23);

  padding-top: 30px;
  padding-bottom: 20px;

  .container{

      font-family: 'Open-sans', sans-serif;
      color: #5A5555;
      line-height: 1.4;

      a{
        color: $gakken-red;
      }

      .textwidget{
        margin-top: 10px;
      }
  }
}



.topics-only{
  padding-right: 15px;
}

.topic-article{

  section:not(first-child){
    padding: 20px 0px;
  }

  #content-summary{
    p{
      padding: 10px 0px;
    }

    .list-group-item{
      padding: 15px;
    }
  }

  #achievements{

    .achievements-container{
      background: #ebebeb;
      padding: 20px;

      .achivements-item{
        margin: 0 auto;
        float: none;
        text-align: center;

        img{
          margin: 0 auto;
          padding: 5px;
        }

        p{
          text-align: center;
        }
      }

    }
  }

  #topic-faq{

    .accordion{
      border: 1px solid #ddd;
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
      border-bottom-right-radius: 5px;
      border-bottom-left-radius: 5px;
      margin-top: 10px;
    }

    .accordion-group{

      &:first-child{
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
      }

      .accordion-heading{
          padding: 10px;
          font-family: 'Open-sans', sans-serif;

          a{
            color: #837D7D;
          }

          .fa-caret-right{
              color: $gakken-red;
          }

      }

      .accordion-inner{
        padding: 20px;
        color: #423B3B;
        background: #ddd;
      }
    }
  }
}

.sidebar-container{
  padding-top: 50px;
  padding-right: 0px;

  div{
    padding-bottom: 20px;
  }

  .list-group{
    font-family: 'Open-sans', sans-serif;

    .active{
      background: $gakken-red;
      border: 0px;
      font-size: 102%;
      font-weight: 600;
    }

    a{
      padding: 20px;
      font-family: 'Open Sans', sans-serif;
      font-size: 110%;

      span.description{
        font-size: 85% !important;
        margin: 5px 0px;
      }
    }

  }
}

aside{
  position: relative;
  overflow: hidden;

  img{
    max-width: 100%;
  }

  .sidebar-sub-container{
    .list-group-item{
      border : none;
    }
  }

  .topics-tab{
    width: 12%;

    .list-group-item{
      background: #E4E4E4;

      &:hover{
        background: #fafafa;
        color: #333;
      }

      .btn{
        background: #1488CC;
        border: #0974B2;
        width: 100%;
      }
    }

    .active{
      background: #fafafa !important;
      color: #333;
      font-weight: 500 !important;
    }

    .fa-caret-right{
      position: absolute;
      right: 10px;
      color: $gakken-red ;
    }
  }

}

.navdis{
  display: block;
}

@media only screen and (min-width : 0px) and (max-width : 786px){

  .slider-sidebar{
    padding-top: 20px;
    padding-left: 0px;
  }

  .nav.navbar-nav.navbar-right{
    text-align: right;
    padding-right: 20px;
  }

  .affix{
    position: relative;
  }

  aside {
    padding-right: 0px !important;
    padding-left: 0px !important;

    .topics-tab{
      width: 100%;
    }
  }
}
