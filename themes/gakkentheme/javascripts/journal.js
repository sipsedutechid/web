angular.module('journalApps', ['infinite-scroll', 'angular-loading-bar'])
.controller('journalCtrl', function($scope, $http) {
    var self = this;

    self.is_loading = false;
    self.is_finished = false;
    $scope.journal = {
        'keyword'   : null ,
        'abjad'     : '#' ,
        'category'  : null ,
        'specialist': null
    }

    self.load = function(url, abj, keyword, category, specialist) {
        if (self.is_finished) return;

        abj         = (abj == '#') ? null : abj;
        category    = (category == '#') ? null : category;
        specialist  = (specialist == '#') ? null : specialist;
        keyword     = keyword ? keyword : null;

        self.is_loading = true;
        $http.post(url, {
            'limit'     : 30 ,
            'abjad'     : abj ,
            'keyword'   : keyword ,
            'category'  : category ,
            'specialist': specialist
        })
        .then(function(response) {
            if (response.data.current_page == '1') {

                self.journals   = response.data;

            } else {

                self.journals.next_page_url = response.data.next_page_url;

                response.data.data.forEach(function (item) {
                    self.journals.data.push(item);
                });
            }

            self.is_finished = self.journals.next_page_url == null;
            self.is_loading = false;

            // $('[data-toggle="tooltip"]').tooltip();
        });
    }

    self.next = function () {
        if (self.is_loading) return;
        self.load(self.journals.next_page_url, $scope.journal.abjad, $scope.journal.keyword, $scope.journal.category, $scope.journal.specialist);
    }

    self.search = function (e, origin) {
        console.log(origin);
        self.is_finished = false;

        if (origin == 'searchbox' || origin == 'btnsearch') {
            $scope.journal.abjad = '#';

            if (
                (
                    origin == 'searchbox' &&
                    ($scope.journal.keyword.length < 3  || e.keyCode != 13)
                ) ||
                (origin == 'btnsearch' && $scope.journal.keyword.length < 3)
            ) return;
        } else if(origin =='abjadm') {
            var radios = document.getElementsByName('abjad');

            radios.forEach(function(item){
                checkUncheck(abjad, $scope.journal.abjad);
            });
        }

        history.pushState('', "Journal", "?");

        if ($scope.journal.keyword != null && $scope.journal.keyword.length > 0) {
            history.pushState('', "Journal", "?q=" + $scope.journal.keyword);
        }

        self.load(url_journal, $scope.journal.abjad, $scope.journal.keyword, $scope.journal.category, $scope.journal.specialist);

    }

    self.removeKeyword = function () {
        checkUncheck(abjad, '#');
        checkUncheck(specialist, '#');
        checkUncheck1(category, '#');
        $scope.journal.keyword = '';

        self.is_finished = false;
        history.pushState('', "Journal", "?");

        self.load(url_journal, '#', null, '#', '#');
    }

    var qvSearch = getQueryVariable('q');

    if (qvSearch) $scope.journal.keyword = qvSearch;

    self.load(url_journal, $scope.journal.abjad, $scope.journal.keyword, $scope.journal.category, $scope.journal.specialist);
});

//dom
var abjad           = document.getElementsByName('abjad') ,
    category        = document.getElementsByName('category') ,
    categoryActive  = document.getElementById('category_active') ,
    categoryOption  = document.getElementsByClassName('cat-option')[0] ,
    categoryIcon    = document.getElementById('icon-choose-category') ,
    specialist      = document.getElementsByName('specialist');

abjad.forEach(function(item){
    item.onclick = function() {
        checkUncheck(abjad, item.value);
    }
});

category.forEach(function(item){
    item.onclick = function() {
        toggleCatMenu();
        checkUncheck1(category, item.value);
        categoryActive.innerHTML = item.value == '#' ? 'Semua' : item.previousSibling.textContent;
    }
});

categoryActive.onclick = function(e) {
    toggleCatMenu();
};

function toggleCatMenu() {
    categoryOption.classList.toggle('hide');
    categoryActive.classList.toggle('active');
    categoryIcon.classList.toggle('fa-chevron-up');
    categoryIcon.classList.toggle('fa-chevron-down');
}

specialist.forEach(function(item){
    item.onclick = function() {
        checkUncheck(specialist, item.value);
    }
});

function checkUncheck (dom, target) {
    dom.forEach(function(item){
        if (item.value == target) item.parentNode.parentNode.classList.add('active');
        else item.parentNode.parentNode.classList.remove('active');
    });
}

function checkUncheck1 (dom, target) {
    dom.forEach(function(item){
        if (item.value == target) item.parentNode.classList.add('active');
        else item.parentNode.classList.remove('active');
    });
}

function getQueryVariable (variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1];}
    }

    return false;
}
