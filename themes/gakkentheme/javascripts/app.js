$(document).ready(function () {
	
	if ($('[data-toggle="tooltip"]').length)
		$('[data-toggle="tooltip"]').tooltip();

	if ($('.navbar').length) {
		_navbar = $('.navbar');
		_wrap = $('.container .wrap');
		$(window).on('scroll', function () {
			_navbar.toggleClass('top', $(this).scrollTop() == 0);
		});
		_navbar.toggleClass('top', $(this).scrollTop() == 0);
	}
	if ($('.navbar-collapse').length) {
		_navbar_overlay = $('.navbar-overlay');
		$('.navbar-collapse').on('show.bs.collapse', function () {
			_navbar_overlay.addClass('in');
		}).on('hidden.bs.collapse', function () {
			_navbar_overlay.removeClass('in');
		});
	}
	if ($('.intermezzo .journals').length) {
		_journals_container = $(this).find('.journals-container');
		_journals_scroll_up = $(this).find('[data-scroll=up]');
		_journals_scroll_down = $(this).find('[data-scroll=down]');
		// console.log(_journals_container.find('.row[data-index=0]'));
		// _journals_container.height(_journals_container.find('.row[data-index=0]').outerHeight() - 10);
		_journals_container.height(525);

		_journals_scroll_up.hide();
		_journals_scroll_down.click(function () {
			if (_journals_container.data('current-index') > 0) return;
			_journals_current_index = _journals_container.data('current-index') + 1;
			_journals_container.attr('data-current-index', _journals_current_index);
			_journals_container.animate({
				scrollTop: _journals_container.find('[data-index]').height()
			}, 1000);
			$(this).slideUp();
			_journals_scroll_up.slideDown();
		});
		_journals_scroll_up.click(function () {
			if (_journals_container.data('current-index') > 0) return;
			_journals_current_index = _journals_container.data('current-index') - 1;
			_journals_container.attr('data-current-index', _journals_current_index);
			_journals_container.animate({
				scrollTop: 0
			}, 1000);
			$(this).slideUp();
			_journals_scroll_down.slideDown();
		});
	}
	if ($('.intermezzo .drugs').length) {
		_drugs_container = $(this).find('.drugs-container');
		_drug_search = $(this).find('#drug-search');
		_drug_classes = _drugs_container.find('.drug-class');
		_drug_classes.find('.class-title').on('click', function (e) {
			e.preventDefault();
			_drug_class = $(this).parent();
			if (_drug_class.hasClass('open')) {
				$(this).blur();
				return _drug_class.removeClass('open').find('.drugs-list').slideUp();
			}
				
			_drugs_container.find('.drug-class.open').removeClass('open').find('.drugs-list').slideUp();
			_drug_class.addClass('open').find('.drugs-list').slideDown();
		});
		_drug_search.on('submit', function (e) {
			e.preventDefault();
			window.location.href = _drug_search.attr('action') + '#/cari?' + _drug_search.serialize();
		});
		_drug_classes.find('.drugs-list').hide();
	}

	var _subscribeForm = $("#newsletter-subscribe");
	if (_subscribeForm.length) {
		var _subscribeUrl = _subscribeForm.attr('action');
		var _subscribeBtn = _subscribeForm.find('#newsletter-subscribe-btn');
		_subscribeBtn.click(function () {
			_subscribeForm.submit();
		});
		_subscribeForm.on('submit', function (e) {
			e.preventDefault();
			$.post(_subscribeUrl, _subscribeForm.serialize());
			_subscribeForm.remove();
			$('#newsletter-subscribe-pre-message').addClass('hidden');
			$('#newsletter-subscribe-post-message').removeClass('hidden');
		});
	}

});