$(document).ready(function() {

 	var hdr = $('.static-nv').height();
      	nvscrolled = "nvscrolled";

    var nav1 = document.getElementById('gakken-nav') ,
        nav2 = document.getElementById('gakken-nav2');

    var val = null;

    $(window).scroll(function() {
    	$('.fixed-nv').toggleClass(nvscrolled, $(this).scrollTop() > hdr);
        findAndOpen( document.body , val );

        if ($(this).scrollTop() <= hdr) closeNav( nav1 );
        else closeNav( nav2 );

  	});

    //set state menu open
    $('.dropdown').on('show.bs.dropdown', function () {
        val = this.querySelector('a').innerHTML;
    });

    $('.dropdown').on('hide.bs.dropdown', function () {
        val = null;
    });

	$('.fixed-nv').toggleClass(nvscrolled, $(this).scrollTop() > hdr);

});

function findAndOpen( elm , val ) {
    elm.querySelectorAll('.dropdown').forEach( function(data) {
        var a = data.querySelector('a');
        if (a.innerHTML == val) {
            data.classList.add('open');
            a.setAttribute('aria-expanded', 'true');
        }
    });
}

function closeNav( elm ) {
    elm.querySelectorAll('.dropdown').forEach( function(data) {
        var a = data.querySelector('a');

        data.classList.remove('open');
        a.setAttribute('aria-expanded', 'false');
    });
}
