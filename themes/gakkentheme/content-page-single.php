<?php
/**
 * The template used for displaying page content
 *
 */
?>


                      <div class="single-article-container" style="margin-top: 20px;">
                        <article>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                        ?>


                        <?php the_content();?>

                        <?php endwhile; endif; ?>
                        </article>
                      </div>
