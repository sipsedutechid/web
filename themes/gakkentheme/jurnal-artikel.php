<?php
    get_header();
    $slug     = urldecode($wp_query->query_vars['journalarticle']);
    $article  = json_decode(do_shortcode('[gkvault-journals-article slug=' . $slug . ']'));
    $vaulturl = do_shortcode('[gkvault-get-base-url]');

    $account    = json_decode( do_shortcode('[gkaccount-login-status]') );
    $date_today = date('Y-m-d H:i:s');
    $subscribe   = false;

    if ($account->status == 'authenticated' && $account->subscription) {
        $subscribe = $account->subscription->expired_at > $date_today;
    }
?>

    <div class="full-width container-fluid">

        <ol class="breadcrumb">
            <li><a href="<?= get_site_url() . "/jurnal" ?>">Jurnal</a></li>
            <li><a href="<?= get_site_url() . "/jurnal/" . $article->journal->slug ?>"> <?= $article->journal->title ?> </a></li>
            <li class="active"><?= $article->title ?></li>
        </ol>

        <div class="main-container">

            <?php if ($subscribe): ?>
                <div class="article-wrapper journal">
                    <aside class=" col-sm-4">

                        <div class="container-aside">
                            <img src="<?= $vaulturl . 'journal/files/image/' . $article->journal->image_url ?>">
                            <ul>
                                <li> <center> <?= $article->journal->title ?> </center> </li>
                            </ul>
                        </div>

                        <div class="container-aside">
                            <?php if (str_replace('-', '0', $article->publish_date) > 0 ): ?>
                                <h2>Dipublikasikan pada </h2>
                                <ul>
                                    <li> <?= date("Y-m-d", strtotime($article->publish_date)) ?> </li>
                                </ul>
                            <?php endif; ?>
                        </div>

                    </aside>

                    <div class="container col-sm-8">
                        <div class="top-container">
                            <div class="row">

                                <div class="descriptions col-sm-10">
                                    <h1>
                                        <?= $article->title ?>
                                    </h1>
                                </div>

                            </div>
                        </div>

                        <div class="middle-container">
                            <h3>Abstract </h3>
                            <div class="extra">
                                <div>
                                    <?= $article->desc ?>
                                </div>
                            </div>
                        </div>

                        <div class="middle-container">
                            <object data="<?= $vaulturl ?>/journal/files/pdf/<?= $article->file ?>" type="application/pdf">
                                <embed src="<?= $vaulturl ?>/journal/files/pdf/<?= $article->file ?>" type="application/pdf" />
                            </object>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="single-article-container" style="margin-top: 20px;">
                    * Berlangganan <a href='<?= get_site_url() ?>/berlangganan'>disini</a>, untuk mengakses konten ini *
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>
