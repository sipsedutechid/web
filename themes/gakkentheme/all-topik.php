<?php
    /*Template Name : Topik page */
    get_header();
 ?>

<?php
    $topic_account  = json_decode(do_shortcode('[gkaccount-subscribe-topics]'));
    $account        = json_decode(do_shortcode('[gkaccount-login-status]'));

    if ($account->status && $account->subscription) {
        $topics = array_merge($topic_account->accessible, $topic_account->backnumbers);

        if (count($topic_account->backnumbers) == 0) $topic_backnumbers = json_decode(do_shortcode('[gkvault-topics-list-by-option category=backnumber]'));
        else $topic_backnumbers = [];

    } else {

        $topics = $topic_account->accessible;
        $topic_backnumbers   = [];

    }
?>

    <div class="full-width container-fluid">
        <div class="row">
            <div class="main-container" style="margin-top: 20px;">
                <div class="article-wrapper col-sm-9">

                    <div class="article-container">
                        <h1 class="sub-title"> <span style="color: #ff0000 !important; "> <?= ($account->status && $account->subscription) ? 'Topik Saya' : 'Topik Dokter' ?> </span>  </h1>

                        <div class="row padding-gap">
                            <?php $i = 1; foreach ($topics as $topic): ?>
                                <?php if (($account->status && $account->subscription) || ($topic->category == 'doctor')): ?>

                                    <?php if ($i >= 4): ?> </div> <div class="row padding-gap"> <?php $i = 1; endif; ?>

                                    <div class=" article-row col-sm-4">
                                        <div
                                            onclick='javascript:location.href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"'
                                            class="article-img"
                                            style="cursor:pointer; <?=
                                                !empty($topic->featured_image) ? 'background: url(\''. do_shortcode('[gkvault-get-base-url]') . '/topic/files/' . $topic->featured_image . '\') center center no-repeat; background-size: cover;' : '' ?>">
                                            <span class="category-highlight"> <?= date('d M Y', strtotime($topic->publish_at)) ?> </span>
                                        </div>
                                        <h3> <a href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"> <?= $topic->title ?> </a> </h3>
                                        <h4 style="font-size: 90%;">
                                            <?php foreach ($topic->lectures as $lecture): ?>
                                                <?= $lecture->front_title . ' ' . $lecture->name . ', ' . $lecture->back_title ?> <br>
                                            <?php endforeach; ?>
                                        </h4>
                                    </div>

                                <?php $i ++; endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <?php if ( !$account->status || !$account->subscription ): ?>
                        <div class="article-container span-big">
                            <h1 class="sub-title"> <span style="color: #237cba !important;"> Topik Dokter Gigi </span>  </h1>

                            <div class="row padding-gap">
                                <?php $i = 1; foreach ($topics as $topic): ?>
                                    <?php if ($topic->category == 'dentist'): ?>

                                        <?php if ($i >= 4): ?>
                                            </div>
                                            <div class="row padding-gap">
                                        <?php $i = 1; endif; ?>

                                        <div class=" article-row col-sm-4">
                                            <div onclick='javascript:location.href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"' class="article-img" style="cursor:pointer; <?= !empty($topic->featured_image) ? 'background: url(\''. do_shortcode('[gkvault-get-base-url]') . '/topic/files/' . $topic->featured_image . '\') center center no-repeat; background-size: cover;' : '' ?>">
                                                <span class="category-highlight"> <?= date('d M Y', strtotime($topic->publish_at)) ?> </span>
                                            </div>
                                            <h3> <a href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"> <?= $topic->title ?> </a> </h3>
                                            <h4 style="font-size: 90%;">
                                                <?php foreach ($topic->lectures as $lecture): ?>
                                                    <?= $lecture->front_title . ' ' . $lecture->name . ', ' . $lecture->back_title ?> <br>
                                                <?php endforeach; ?>
                                            </h4>
                                        </div>
                                    <?php $i ++; endif; ?>

                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (count($topic_backnumbers) > 0): ?>
                        <div class="article-container span-big">
                            <h1> Backnumber </h1>

                            <div class="row">
                                <?php $i = 1; foreach ($topic_backnumbers as $topic): ?>

                                    <?php if ($i >= 4): ?>
                                        </div>
                                        <div class="row padding-gap">
                                    <?php $i = 1; endif; ?>

                                    <div class=" article-row col-sm-4">
                                        <div
                                            class="article-img"
                                            style="<?= !empty($topic->featured_image) ? 'background-image: url(\''. do_shortcode('[gkvault-get-base-url]') . '/topic/files/' . $topic->featured_image . '\') ' : '' ?>">
                                            <span class="category-highlight"> <?= date('d M Y', strtotime($topic->publish_at)) ?> </span>
                                        </div>
                                        <h3> <a href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"> <?= $topic->title ?> </a> </h3>
                                        <h4 style="font-size: 90%;">
                                            <?php foreach ($topic->lectures as $lecture): ?>
                                                <?= $lecture->front_title . ' ' . $lecture->name . ', ' . $lecture->back_title ?> <br>
                                            <?php endforeach; ?>
                                        </h4>
                                    </div>

                                    <?php $i ++; endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php get_sidebar();?>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
