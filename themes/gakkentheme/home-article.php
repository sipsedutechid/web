<?php
    $vault_url = do_shortcode('[gkvault-get-base-url]');
    $topics    = json_decode(do_shortcode('[gkvault-topics-latest]'));
?>

<div class="article-wrapper">
    <div class="article-container">
        <?php require_once 'omnisearch.php'; ?>
    </div>
</div>

<div class="article-wrapper">
    <div class="article-container artikel">
        <h1 class="sub-title"> <span> Artikel Terbaru </span>
            <span class="readmore">
                <a href="index.php/artikel-terbaru"> Read More </a>
            </span>
        </h1>

        <div class="row">
            <?php
                $i=0;
                $query = new WP_Query( array( 'posts_per_page' => 6, 'ignore_sticky_posts' => 1 ) );

                $mypod = pods('post');
                $mypod->find('subheading');
                if( $query->have_posts() ) : while( $query -> have_posts() ) : $query->the_post() ;
            ?>

            <?php if ( $i % 3 === 0 ): ?>
                </div> <div class="row">
            <?php endif; ?>

            <div class=" article-row col-sm-4">
                <div class="article-cont">
                    <?php 
                    $categories = get_the_category($post->ID);
                    $post_is_opini = false;
                    foreach ($categories as $cat) {
                        if ($cat->slug == 'opini') $post_is_opini = true;
                    }
                    ?>
                    <?php if($post_is_opini): ?>
                        <div class="badge-opini text-center" style="position:absolute;top:-5px;right:15px;padding:10px 15px;background:#A41E22;text-transform:uppercase;box-shadow:0 2px 2px rgba(0,0,0,0.2);color:#fff;border-bottom-left-radius:4px;border-bottom-right-radius:4px;">
                            <b class="fa fa-fw fa-star fa-2x"></b><br />
                            Opini
                        </div>
                    <?php endif ?>

                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );?>
                    <a href="<?php the_permalink();?>"
                        title="<?php the_title();?>"
                        style="background: url('<?= $thumb['0'];?>') no-repeat; background-size: cover;background-position: 50% 50%;display: block; height: 200px; width: auto; "
                        alt="<?php the_title();?>">
                    </a>

                    <div class="article-p">
                        <h3 style="border-bottom: 1px solid #ebebeb; padding-bottom: 15px;">
                            <a href="<?php the_permalink();?>" alt="<?php the_title();?>">
                                <?php the_title();?>
                            </a>
                        </h3>

                        <?php $subheading = get_post_meta($post->ID, 'subheading', true); ?>

                        <?php if (!empty($subheading)): ?>
                            <div class="post-subheading">
                                <?php echo apply_filters('the_excerpt', $subheading); ?>
                            </div>
                            <p class="text-right">
                                <a href="<?php the_permalink();?>" style="color:#A41E22;">Baca Selengkapnya <i class="fa fa-arrow-right"></i></a>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php $i++; endwhile; endif;?>
        </div>
    </div>

    <section class="intermezzo article-container">
        <div class="row">
            <div class="col-sm-4 drugs">
                <h1 class="ellipsis sub-title">Indeks Obat</h1>
                <form id="drug-search" accept-charset="utf-8" action="<?php bloginfo('url') ?>/obat">
	                <div class="input-group" style="margin-top: 25px;">
	                    <input type="text" name="q" class="form-control" placeholder="Cari nama dan kategori obat.." />
	                    <span class="input-group-btn">
	                        <button type="submit" class="btn btn-default"><b class="fa fa-search"></b></button>
	                    </span>
	                </div>
	            </form>
                <?php
                $classes = do_shortcode('[gkvault-drug-classes-list]');
                $classes = json_decode($classes);
                ?>

                <div class="drugs-container">
                    <?php foreach($classes as $class): ?>
                        <?php if ($class->drugs_count > 5): ?>
                            <div class="drug-class">
                                <a href="#" class="class-title">
                                    <div class="class-name ellipsis pull-left" style="width:80%;"><?php echo $class->name ?></div>
                                    <div class="drug-count pull-right"><?php echo $class->drugs_count ?> obat</div>
                                    <div class="clearfix"></div>
                                </a>

                                <div class="drugs-list list-group">
                                    <?php foreach($class->drugs as $drug): ?>
                                        <a href="<?php bloginfo('url'); ?>/obat/#/<?php echo $drug->slug ?>" class="list-group-item"><?php echo $drug->name ?> (<?php echo $drug->drug_manufacturer->name ?>)</a>
                                    <?php endforeach ?>
                                    <a href="<?php bloginfo('url'); ?>/obat/#/cari?kategori=<?php echo $class->code ?>" class="list-group-item next"> <?php echo $class->drugs_count - 5 ?> obat lainnya <b class="fa fa-arrow-right fa-fw pull-right"></b></a>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="col-sm-4 journals">
                <h1 class="ellipsis sub-title"><span>Daftar Jurnal</span></h1>

                <button type="button" class="btn btn-block" data-scroll="up" style="margin-bottom: 20px;">
                    <b class="fa fa-angle-up fa-2x"></b>
                </button>

                <div class="journals-container" data-current-index="0">
                    <?php
                        $journals = json_decode(do_shortcode('[gkvault-journals-latest]'));
                        $i = 0;
                        $a = 1;
                    ?>

                    <div class="row" data-index="0">
                        <?php foreach ($journals as $journal): ?>

                            <?php if ($i == 4): ?>
                                </div><div class="row" data-index="<?= $a ?>">
                            <?php $i = 0; $a++; endif; ?>

                            <div class="col-sm-6">
                                <a href="https://gakken-idn.id/jurnal/<?php echo $journal->slug ?>/" class="thumbnail thumbnail-journal">
                                    <div class="image"><img src="<?= $vault_url . 'journal/files/image/' . $journal->image_url ?>" /></div>
                                    <div class="caption">
                                        <div class="ellipsis"><strong><?= $journal->title ?></strong></div>
                                    </div>
                                </a>
                            </div>
                        <?php $i++; endforeach; ?>
                    </div>

                </div>
                <button type="button" class="btn btn-block" data-scroll="down" style="margin-top: 15px;">
                    <b class="fa fa-angle-down fa-2x"></b>
                </button>

                <div style="text-align: center; padding: 20px; "> <a href="https://gakken-idn.id/jurnal" style="font-weight: 600; color: #A41E22 !important;"> Semua Jurnal <i class="fa fa-arrow-right"> </i> </a> </div>
            </div>

            <div class="col-sm-4">
                <div>
                    <h1 class="ellipsis sub-title"> <span> Artikel Terpopuler </span>  </h1>
                    <div class="main-banner popular-posts" id="popular-post">
                        <?php
                            $args = array(
                                 'limit' => 5,
                                 'range' => 'monthly',
                                 'freshness' => 1,
                                 'post_type' => 'post',
                                 'order_by' => 'views',
                                 'stats_views' => 0,
                                 'wpp_start' => '<ul>',
                                 'wpp_end' => '</ul>',
                                 'post_html' => '<li class="row">
                                                    <div class="number-container">
                                                        <a class="article-img increment" href="{url}" style="background:#A41E22 no-repeat;background-size:cover;background-position:50% 50%;width:80%;height:40px;display:block;text-align:center;color:#fff;padding:10px;margin-left:15px;">

                                                        </a>
                                                    </div>
                                                    <div class="headlines-container"><a href="{url}">{text_title}</a></div>
                                            </li>'
                            );

                            wpp_get_mostpopular($args);?>
                        </div>

                        <script type="text/javascript">
                            var popularPostElm = document.getElementById('popular-post');
                            var elmInc = popularPostElm.getElementsByClassName('increment');
                            var i = 1;

                            for (var index in elmInc) {
                                if (elmInc.hasOwnProperty(index)) {
                                    elmInc[index].innerHTML = i;
                                    i++;
                                }
                            }

                        </script>
                </div>

                <div style="padding: 40px 0px;">
                    <h1 class="ellipsis sub-title"> <span> Artikel Pilihan </span>  </h1>
                    <div class="main-banner popular-posts">
                        <ul>
                            <?php if ( have_posts() && query_posts('showposts=4') ) : while ( have_posts() ) : the_post();?>

                                <?php if( is_sticky() ):?>

                                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
                                    <li class="row" style="margin: 0 auto; ">
                                        <div class="number-container">
                                            <a class="article-img"
                                                href="<?php the_permalink();?>"
                                                style="background: url('<?php echo $thumb['0'];?>') no-repeat; background-size: cover; background-position: 50% 50%; width: 100%; height: 70px; display: block;">
                                            </a>
                                        </div>

                                        <div class="headlines-container "> <a href="<?php the_permalink();?>"> <?php the_title();?> </a> </div>
                                    </li>

                                <?php endif;?>
                            <?php endwhile; endif;?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?php if (false): // change to true for live streaming sessions ?>
        <section class="article-container">
            <h1 class="sub-title"> <span> Live Streaming: Gakken E-Learning Site Launch</span> </h1>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://web-seminar.gakken-idn.id/live/ap/65b1728c982a8658aa5767c1c86da234fecfe8e4"></iframe>
            </div>
        </section>
    <?php endif; ?>

    <div class="row full-width">
        <div class="article-container">
            <div class="row">
                <div class="article-wraps col-sm-6">
                    <h1 class="sub-title"> <span> Topik Dokter Terbaru </span> <span class="readmore"> <a href="https://gakken-idn.id/topik"> Read More </a> </span> </h1>

                    <div class="row">
                        <?php $i = 0; foreach ($topics->doctor as $topic): ?>
                            <?php if ($i === 2): ?>
                                </div><div class="row">
                            <?php $i = 0; endif; ?>
                            <div class=" article-row col-sm-6">
                                <div class="article-cont">
                                    <a class="article-img"
                                        href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"
                                        style="background: url('<?= $vault_url . '/topic/files/' . $topic->featured_image ?>') no-repeat; background-size: cover; background-position: 50% 50%; width: 100%; height: 185px; display: block;">
                                        <?php if (isset($topic->tags[0])): ?>
                                            <span class="category-highlight"> <?= date('d M y', strtotime($topic->publish_at)) ?> </span>
                                        <?php endif; ?>
                                    </a>

                                    <div class="article-p">
                                        <h3> <a href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"> <?= $topic->title ?> </a> </h3>
                                        <h4 class="names">
                                            oleh:
                                            <?php $a = 0; foreach ($topic->lectures as $lecture): ?>
                                                <?php if ($a > 0): ?>,&bsp;<?php endif; ?>
                                                <?= $lecture->front_title . ' ' . $lecture->name . ', ' . $lecture->back_title ?>
                                            <?php $a++; endforeach; ?>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>
                    </div>

                </div>

                <div class="article-wraps col-sm-6">
                    <h1  class="sub-title" > <span style="color: #237cba !important;"> Topik Dokter Gigi Terbaru </span> <span class="readmore"> <a href="https://gakken-idn.id/topik"> Read More </a> </span> </h1>

                    <div class="row">
                        <?php $i = 0; foreach ($topics->dentist as $topic): ?>
                            <?php if ($i === 2): ?>
                                </div><div class="row">
                            <?php $i = 0; endif; ?>
                            <div class=" article-row col-sm-6">
                                <div class="article-cont">
                                    <a class="article-img"
                                        href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"
                                        style="background: url('<?= $vault_url . '/topic/files/' . $topic->featured_image ?>') no-repeat; background-size: cover; background-position: 50% 50%; width: 100%; height: 185px; display: block;">
                                        <?php if (isset($topic->tags[0])): ?>
                                            <span class="category-highlight"> <?= date('d M y', strtotime($topic->publish_at)) ?> </span>
                                        <?php endif; ?>
                                    </a>

                                    <div class="article-p">
                                        <h3> <a href="<?= bloginfo('url') . '/topik/judul/' . $topic->slug ?>"> <?= $topic->title ?> </a> </h3>
                                        <h4 class="names">
                                            oleh :
                                            <?php $a = 0; foreach ($topic->lectures as $lecture): ?>
                                                <?php if ($a > 0): ?>,&bsp;<?php endif; ?>
                                                <?= $lecture->front_title . ' ' . $lecture->name . ', ' . $lecture->back_title ?>
                                            <?php $a++; endforeach; ?>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
