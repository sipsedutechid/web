<?php
$page_title = 'Jurnal Gakken';
    get_header();

    $vault_url          = do_shortcode('[gkvault-get-base-url]');
    $journal_category   = json_decode(do_shortcode('[gkvault-get-journal-category]'));

?>
<div class="full-container journal-intro" style="font-family: 'Roboto', sans-serif;">
    <div class="full-width container-fluid row">
        <div class="col-sm-4 image-wrap">
            <img src="<?= get_template_directory_uri() . '/images/gakken-journal.png' ?>" />
        </div>
        <div class="col-sm-8 desc">
            <span class="title"> Jurnal </span>
            Jurnal kesehatan berkualitas disediakan Gakken melalui layanan jurnal terpadu oleh <a style="font-weight:bold; color: #fff;">Wiley</a>,
            kepada seluruh pengguna berlangganan. Pencarian dari ratusan jurnal, artikel dan e-book dapat dilakukan dengan sangat mudah dan intuitif.
            Kumpulan jurnal juga dikaitkan dengan topik pembelajaran P2KB, sehingga memudahkan pengguna mengakses publikasi yang relevan ketika sedang belajar.
        </div>
    </div>
</div>
<div class="full-width container-fluid" style="font-family: 'Roboto', sans-serif;" ng-app='journalApps'>
        <div style="margin-top:20px; display:block; height: 100%; width:100%;" ng-controller='journalCtrl as journalcon'>
            <div class="nav-abjad-journal">
                <ul>
                    <li></li>
                    <li></li>
                    <li class="active">
                        <label> #
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="#" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> A
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="a" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> B
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="b" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> C
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="c" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> D
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="d" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> E
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="e" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> F
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="f" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> G
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="g" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> H
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="h" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> I
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="i" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> J
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="j" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> K
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="k" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> L
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="l" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> M
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="m" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> N
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="n" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> O
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="o" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> P
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="p" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> Q
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="q" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> R
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="r" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> S
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="s" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> T
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="t" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> U
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="u" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> V
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="v" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> W
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="w" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> X
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="x" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> Y
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="y" class="hide">
                        </label>
                    </li>
                    <li>
                        <label> Z
                            <input type="radio" name="abjad" ng-model="journal.abjad" ng-click="journalcon.search($event, 'abjad');" value="z" class="hide">
                        </label>
                    </li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
            <div class="article-wrapper journal-wraps">
                <div class="row nav-journal">
                    <div class="col-sm-9">
                        <ul class="browse-journal">
                            <li class="active">
                                <label> Semua
                                    <input type="radio" name="specialist" ng-model="journal.specialist" ng-click="journalcon.search($event, 'specialist');" class="hide" value="#">
                                </label>
                            </li>
                            <li>
                                <label> Dokter
                                    <input type="radio" name="specialist" ng-model="journal.specialist" ng-click="journalcon.search($event, 'specialist');" value="doctor" class="hide">
                                </label>
                            </li>
                            <li>
                                <label> Dokter Gigi
                                    <input type="radio" name="specialist" ng-model="journal.specialist" ng-click="journalcon.search($event, 'specialist');" value="dentist" class="hide">
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div class="col-sm-3 search-container">
                        <input type="text" name="journal_keyword" ng-model="journal.keyword" ng-keyup="journalcon.search($event, 'searchbox')" class="searchbox-underline" placeholder="Mencari Jurnal...">
                        <button type="button" ng-click="journalcon.removeKeyword()" name="bnt_remove_keyword" class="btn-transparant" ng-show="journal.keyword.length > 0">
                            <span class="fa fa-remove"></span>
                        </button>
                        <button type="button" ng-click="journalcon.search($event, 'btnsearch')" name="btn_search_journal" class="btn-transparant">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px; margin-bottom: 10px; position:relative">
                    <div class="">
                        Kategori: <span id="category_active">Semua</span><i class="fa fa-chevron-down" aria-hidden="true" id="icon-choose-category"></i>
                    </div>
                    <div class="cat-option hide">
                        <div class="col-sm-3">
                            <?php
                                $i = 0;
                                $max_col = count($journal_category) > 4 ? ceil(count($journal_category) / 4) : 1;
                            ?>
                            <label> Semua
                                <input type="radio" name="category" ng-model="journal.category" ng-click="journalcon.search($event, 'category');" value="#" class="hide" checked>
                            </label>
                            <?php foreach ($journal_category as $data): ?>
                                <?php if ($max_col == $i): ?>
                                    </div><div class="col-sm-3">
                                <?php $i = 0; endif; ?>

                                <label> <?= $data->labels ?>
                                    <input type="radio" name="category" ng-model="journal.category" ng-click="journalcon.search($event, 'category');" value="<?= $data->slug ?>" class="hide">
                                </label>

                            <?php $i++; endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- <div class="journal-slider">
                    <div class="journal-wrap">
                            <?php //$i = 1; foreach ($latest_journal as $journal): ?>
                            <div class="slider-wrap" style="margin-top: 0px">
                                <div class="slider-journal">
                                    <div class="image">
                                        <img src="<?php //$vault_url . 'journal/files/image/' . $journal->image_url ?>" />
                                    </div>

                                    <div class="title">
                                        <a href="<?php //bloginfo('url') . '/jurnal/' . $journal->slug ?>">
                                            <h2> <?php //$journal->title ?> </h2>
                                        </a>
                                    </div>
                                </div>
                            <?php //$i++; endforeach; ?>
                        </div>
                    </div>
                    <div class="slider-journal-nav">
                        <ul id="navJournal">
                        </ul>
                    </div>
                </div> -->

                <!-- <div class="search" style="margin-top: 20px;">
                    <div class="input-group stylish-input-group">
                        <input type="text" class="form-control"  placeholder="Search" >
                        <span class="input-group-addon">
                            <button type="submit">
                                <span class="fa fa-search"></span>
                            </button>
                        </span>
                  </div>
                </div> -->

                <!-- <div class="list-containers">
                    <div class="line-spacing">

                        <?php
                            //$i = 0;
                            // foreach ($journals->list as $journal):
                            //     $abjad = strtoupper(substr($journal->title, 0, 1))
                        ?>

                            <?php //if ($i == 0): ?>
                                <div class="headline">
                                    <h2> <?php // $abjad ?>  </h2>
                                </div>
                                <div class="filler">
                                    <ul>
                            <?php //elseif( strtoupper(substr($journals->list[$i-1]->title, 0, 1)) != $abjad): ?>
                                    </ul>
                                </div>
                                <div class="headline">
                                    <h2> <?php //$abjad ?>  </h2>
                                </div>
                                <div class="filler">
                                    <ul>
                            <?php //endif; ?>
                            <li>
                                <a href="<?php //bloginfo('url') . '/jurnal/' . $journal->slug ?>"> <?php //$journal->title ?> </a>
                            </li>
                        <?php //$i++; endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="">
                        <?php //foreach ($pagination as $data): ?>
                            <?php //if ($page == $data): ?>
                                <?php //$data ?>
                            <?php //else: ?>
                                <a href="?page=<?php //$data ?>">
                                    <?php //$data ?>
                                </a>
                            <?php //endif; ?>
                        <?php //endforeach; ?>
                    </div>
                </div>
            </div> -->

                <div class="list-containers row" infinite-scroll="journalcon.next()">
                        <div class="col-sm-2" ng-repeat="journal in journalcon.journals.data">
                            <div class="journal-container">
                                <div class="cover">
                                    <a href="<?= bloginfo('url') . '/jurnal/' ?>{{ journal.slug }}">
                                        <img ng-src="<?= $vault_url . 'journal/files/image/' ?>{{ journal.image_url }}" alt="{{ journal.title }}" />
                                    </a>
                                </div>
                                <div class="title">
                                    <a href="<?= bloginfo('url') . '/jurnal/' ?>{{ journal.slug }}" data-toggle='tooltip' data-placement="top" title="{{ journal.title }}"> {{ journal.title }} </a>
                                </div>
                                <div class="sub-title ellipsis">
                                    {{ journal.publisher }}
                                </div>
                                <div class="issn ellipsis">
                                    ISSN {{ journal.issn }} <br>
                                    E-ISSN {{ (journal.e_issn ? journal.e_issn : '-') }}
                                </div>
                            </div>
                        </div>
                </div>

                <div class="text-center" ng-show="journalcon.is_loading" style="margin:45px 0;">
                    <b class="fa fa-circle-o-notch fa-spin fa-2x"></b>
                    <p style="margin-top:5px;">Memuat..</p>
                </div>
                <div class="text-center" ng-if="!journalcon.journals.data.length && !journalcon.is_loading" style="margin:45px 0;">
                    <b class="fa fa-frown-o fa-5x"></b>
                    <p style="margin-top:5px;">Jurnal yang Anda cari tidak dapat ditemukan</p>
                </div>

                <?php //get_sidebar();?>
                 <div style="clear: both;"></div>
              </div>
        </div>
  </div>
      <!-- <script src="<?php //bloginfo('template_directory') ?>/javascripts/slider-journal.js" charset="utf-8"></script> -->
      <?php get_footer(); ?>

    <script type="text/javascript">
        var jounalTextBox = document.getElementsByName('journal_keyword')[0];
        var journalSearch = document.getElementsByClassName('search-container')[0];

        jounalTextBox.onfocus = function() {
            journalSearch.classList.add('active');
        }

        jounalTextBox.onfocusout = function() {
            journalSearch.classList.remove('active');
        }
    </script>
    <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/angular.min.js"></script>
    <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/loading-bar.js"></script>
    <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/ng-infinite-scroll.min.js"></script>
    <script type="text/javascript">
        var url_journal = "<?= $vault_url . 'api/journal/list' ?>";
    </script>
    <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/journal.js"></script>
