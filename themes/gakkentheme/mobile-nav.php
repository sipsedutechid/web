<nav class="navbar navbar-default fixed-nv">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="logo"> <a class="navbar-brand" href="#"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png" /> </a> </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Artikel Kesehatan <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Berita</a></li>
            <li><a href="#">Darah / Hematologi </a></li>
            <li><a href="#"> Gigi </a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Kehamilan / Obstetri </a></li>
            <li><a href="#"> Kesehatan Anak </a></li>
            <li><a href="#"> Penyakit </a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"> Tips Kesehatan </a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Referensi <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Topik <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>

        <li><a href="#">Unduh</a></li>
        <li><a href="#">Tentang Kami</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown user-profile-wrap">
          <button href="#" class="dropdown-toggle user-profile-wrap-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><div class="user-profile" style="background: url('images/user.jpg') no-repeat; background-size: cover;
          background-position: 50% 50%;"></div> </button>
          <ul class="dropdown-menu user-tab">
            <li><a href="#">My Account</a></li>
            <li><a href="#">My Subscriptions</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Sign Out</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
