<?php
/*Template Name : 404 Forbidden*/

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width,initial-scale=1" />
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
		<meta charset="utf-8" />

		<?php
		$title = "";
		if (!is_home()) {
			if (is_search()) $title = get_search_query();
			elseif (is_category()) $title = single_cat_title('', false);
			elseif (is_tax()) $title = single_cat_title('', false);
			else $title = get_the_title();
		}
		?>

		<title><?php if(!empty($title)): echo $title.' | Gakken Indonesia '; else: bloginfo('site_title'); endif; ?> </title>


		<?php if (isset($wp_query->query_vars['drug'])): ?>
			<base href="/obat/" />
		<?php endif ?>

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
		<![endif]-->

		<link href="<?php bloginfo('stylesheet_url'); ?>" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/ie.css" media="print" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/journal-slider.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/loading-bar.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/journal.css" media="screen" rel="stylesheet" type="text/css" />
		<link href="<?php echo esc_url(get_template_directory_uri()); ?>/stylesheets/mediadata.css" media="screen" rel="stylesheet" type="text/css" />
		<link rel="alternate" type="application/rss+xml" title="RSS	2.0" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico" />

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '1060121160748989',
					xfbml      : true,
					version    : 'v2.6'
				});
			};

			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) { return; }
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			} (document, 'script', 'facebook-jssdk'));
		</script>

		<style>
			ul li{
				padding: 5px 0px;
			}

			ul li a{
				color: #3798d3;
			}

			.pseudo-nav img{
				height: 50px;
			}
		</style>

	</head>
	<body>



    <div class="container-fluid full-width">

    	<div class="pseudo-nav col-sm-12">
    	<a href="http://gakken-idn.id"> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-gakken-indonesia.png" /> </a>
    	</div>

          
      	<div style="margin-top: 20px;">

      			<div class="row">

      			<div class="col-sm-4">
      				<div style="padding: 100px 20px">
      						<p style="font-size: 2em; font-weight: 600; "> 404 : Page Not Found </p>
                       <p> Maaf, konten yang anda cari tidak ditemukan. </p>
                       <p style="padding-top: 20px;"> Kembali ke </p>
                       <p>
                       	<ul> 
                       		<li> <a href="https://gakken-idn.id"> Beranda </a> </li>
                       		<li> <a href="https://gakken-idn.id/topik"> Topik </a> </li>
                       		<li> <a href="https://gakken-idn.id/obat"> Obat </a> </li>
                       		<li> <a href="https://gakken-idn.id/berlangganan"> Berlangganan </a> </li>
                       	</ul>
                       </p>
                        
                    </div>
				</div>

						<div class="col-sm-8">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/404.png" />
						</div>
				</div>

        </div>

            

        
    </div>

		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/bootstrap.min.js" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/bootstrap-sprockets.js" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/bootstrap/tooltip.js" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/app.js?ver=1.1" crossorigin="anonymous"></script>
		<script src="<?php echo esc_url(get_template_directory_uri()); ?>/javascripts/active-menu.js" crossorigin="anonymous"></script>
		<script id="dsq-count-scr" src="//gakken-indonesia.disqus.com/count.js?https" async></script>
		<?php do_shortcode('[gakken-accounts-script]'); ?>
	</body>
</html>