<?php 
$account = json_decode(do_shortcode('[gkaccount-login-status]'));
$status = 'unauthenticated';
if ($account->status == 'authenticated') {

  $status = 'unsubscribed';

  if ($account->subscription) {
    if (
      time() > strtotime($account->subscription->starts_at)
      && time() < strtotime($account->subscription->expired_at)
      )
      $status = 'subscribed';
  }
}

switch ($status) {
  case 'subscribed': $subscribe_url = gakken_accounts_get_base_url() . 'my/subscriptions'; break;
  case 'unsubscribed': $subscribe_url = gakken_accounts_get_base_url() . 'subscribe'; break;
  default: $subscribe_url = gakken_accounts_get_base_url() . 'register';
}
?>

    <div class="subscribe">

        <div class="subscribe-bg" style="background: url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/cover-dark-2.jpg') no-repeat; background-size: cover;
        background-position: 50% 50%;">

        </div>

        <div class="copywrite container">
          <center> <h1> Berlangganan di Gakken </h1> </center>
          <center> <p> Dapatkan akses penuh untuk setiap konten, topik, jurnal dan repository obat. </p> </center>
        </div>

        <div class="subscribe-box container">
          <div class="row">
            <div class="col-sm-4">
              <div class="bottom">
                <div class="top-part">
                  <center> <h1> Langganan 3 Bulan </h1> </center>
                </div>
                <div class="middle-part">
                  <h4> Rp 2.000.000,- </h4>

                  <a href="<?php echo $subscribe_url . ($status == 'unsubscribed' ? '?sub=3month' : ''); ?>" class="btn btn-default btn-load">
                    Berlangganan
                  </a>
                </div>
                <div class="bottom-part">
                  <ul>
                    <li> Akses penuh ke 3 Topik Pembelajaran </li>
                    <li> Artikel dan Jurnal Medis </li>
                    <li> Indeks Obat, Dokter dan Rumah Sakit/Klinik </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="middle">
                <div class="top-part">
                  <center> <h1> Langganan 6 Bulan </h1> </center>
                </div>
                <div class="middle-part">
                  <h4> Rp 4.000.000,- </h4>

                  <a href="<?php echo $subscribe_url . ($status == 'unsubscribed' ? '?sub=6month' : ''); ?>" class="btn btn-default btn-load">
                    Berlangganan
                  </a>
                </div>
                <div class="bottom-part">
                  <ul>
                    <li> Akses penuh ke 6 Topik Pembelajaran </li>
                    <li> Artikel dan Jurnal Medis </li>
                    <li> Indeks Obat, Dokter dan Rumah Sakit/Klinik </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="top-border">
                <div class="top-part">
                  <center> <h1> Langganan 1 Tahun </h1> </center>
                </div>
                <div class="middle-part">
                  <h4> Rp 8.000.000,- </h4>

                  <a href="<?php echo $subscribe_url . ($status == 'unsubscribed' ? '?sub=yearly' : ''); ?>" class="btn btn-default btn-load">
                    Berlangganan
                  </a>
                </div>
                <div class="bottom-part">
                  <ul>
                    <li> Akses penuh ke 12 Topik Pembelajaran </li>
                    <li> Artikel dan Jurnal Medis </li>
                    <li> Indeks Obat, Dokter dan Rumah Sakit/Klinik </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="subscribe-desc grey-bg">
          <div class="description container">
            <h1 class="title"> Berlangganan di Gakken </h1>
            <p class="title"> Dengan berlangganan, Anda akan mendapatkan keuntungan-keuntungan dibawah ini : </p>
            <div class="row">
              <div class="col-sm-6">
                <div class="img col-md-6 text-center">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon-vector_03.png" style="max-width:300px;">
                </div>
                <div class="desc col-md-6">
                  <h2> Akses penuh ke Topik Pembelajaran </h2>
                  <p> Belajar melalui beragam materi pembelajaran dalam konten-konten dokumen, audio dan video, yang dapat diunduh kapan dan dimana saja. </p>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="img col-md-6 text-center">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon-vector_07.png" style="max-width:300px;">
                </div>
                <div class="desc col-md-6">
                  <h2> Sertifikasi P2KB </h2>
                  <p> Melalui tes online, dapatkan sertifikat P2KB Gakken yang dapat Anda gunakan untuk memperbarui SIP. </p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="img col-md-6 text-center">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon-vector_04.png" style="max-width:300px;">
                </div>
                <div class="desc col-md-6">
                  <h2> Artikel dan Jurnal Medis </h2>
                  <p> Kami menyediakan artikel dan jurnal medis dari seluruh dunia yang diperbaharui sesering mungkin untuk menambah referensi dan pengetahuan Anda sehubungan dengan Topik. </p>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="img col-md-6 text-center">
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon-vector_09.png" style="max-width:300px;">
                </div>
                <div class="desc col-md-6">
                  <h2> Indeks Obat, Dokter dan Rumah Sakit/Klinik</h2>
                  <p> Informasi obat, dokter dan rumah sakit/klinik yang lengkap dan terbaru, dapat dengan mudah dicari untuk apapun kebutuhan Anda. </p>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="subscribe-desc">
          <div class="description">
          <center> <h1 class="title"> Gakken telah bekerjasama dengan : <h2>  </center>
          </div>
          <div class="container">
            <div class="row">
              <div class="picture-wrap">
                <div class="partner-img col-sm-3"></div>
                <div class="partner-img col-sm-2"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/unhas-logo.png"><p> Universitas Hasanuddin </p></div>
                <div class="partner-img col-sm-2"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/idi-logo.png"><p> IDI </p> </div>
                <div class="partner-img col-sm-2"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/PDGI.png"><p> PDGI </p> </div>
                <div class="partner-img col-sm-3"></div>
              </div>
            </div>
          </div>
        </div>

    </div>
