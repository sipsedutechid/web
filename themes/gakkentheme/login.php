<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> Gakken Health and Education </title>
    <!-- Latest compiled and minified CSS -->

    <link href="stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
    <link href="stylesheets/styles.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/screen.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/ie.css" media="print" rel="stylesheet" type="text/css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->

  </head>
  <body data-spy="scroll" data-target="#sidebar-scroll">

    <nav class="navbar navbar-default fixed-nv">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="logo"> <a class="navbar-brand" href="#"> <img src="images/logo.png" /> </a> </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown active">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Artikel Kesehatan <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Berita</a></li>
                <li><a href="#">Darah / Hematologi </a></li>
                <li><a href="#"> Gigi </a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Kehamilan / Obstetri </a></li>
                <li><a href="#"> Kesehatan Anak </a></li>
                <li><a href="#"> Penyakit </a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#"> Tips Kesehatan </a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Referensi <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Topik <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>

            <li><a href="#">Unduh</a></li>
            <li><a href="#">Tentang Kami</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li><button type="button" class="btn btn-default navbar-btn highlight-btn ">Sign in</button></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="full-container static-nv-container">
      <div class="full-width">
        <nav class="navbar navbar-default static-nv">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div class="logo"> <a class="navbar-brand" href="#"> <img src="images/logo.png" /> </a> </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
              <ul class="nav navbar-nav">
                <li class="dropdown active">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Artikel Kesehatan <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Berita</a></li>
                    <li><a href="#">Darah / Hematologi </a></li>
                    <li><a href="#"> Gigi </a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Kehamilan / Obstetri </a></li>
                    <li><a href="#"> Kesehatan Anak </a></li>
                    <li><a href="#"> Penyakit </a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#"> Tips Kesehatan </a></li>
                  </ul>
                </li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Referensi <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Obat</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Jurnal</a></li>
                  </ul>
                </li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Topik <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>

                <li><a href="#">Unduh</a></li>
                <li><a href="#">Tentang Kami</a></li>
              </ul>

              <ul class="nav navbar-nav navbar-right">
                <li><button type="button" class="btn btn-default navbar-btn highlight-btn ">Sign in</button></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
      </div>
    </div>

    <div class="full-width container-fluid">

        <div class="main-container">
          <div class="login-wrapper">
            <div class="login-form col-sm-4">
              <h3> Sign in to your Account </h3>
              <h4> Don't have account? <a href="#"> Sign up </a> </h4>

              <div class="login-container">
                <form>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>

                  <button type="submit" class="btn btn-default">Submit</button>
                </form>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>



    <footer class="footer">
      <section id="footer">
        <div class="container">
          <div class="row hidden-xs">

            <div class="col-sm-4 hidden-sm hidden-xs">
              <img src="images/logo.png" style="width:100%;max-width:200px;" />
              <p class="small" style="margin-top:15px;">
                PT. Gakken Health and Education Indonesia<br />
                Jl. Sastra I Blok A No. 28<br />
                Makassar, Indonesia
              </p>
              <p class="small">contact@eduhealth.co.id</p>
            </div>

            <div class="col-md-2 col-sm-3 widget"><h3>Kesehatan</a> </h3><div class="menu-footer-links-health-container"><ul id="menu-footer-links-health" class="menu"><li id="menu-item-119" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119"><a href="https://eduhealth.co.id/artikel/">Artikel</a></li>
            <li id="menu-item-120" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-120"><a href="https://eduhealth.co.id/jurnal/">Jurnal</a></li>
            <li id="menu-item-122" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-122"><a href="https://eduhealth.co.id/unduh/">Unduhan</a></li>
            <li id="menu-item-121" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-121"><a href="https://eduhealth.co.id/obat/">Indeks Obat</a></li>
          </ul></div></div><div class="col-md-2 col-sm-3 widget"><h3>Kerjasama</a> </h3><div class="menu-footer-links-external-container"><ul id="menu-footer-links-external" class="menu"><li id="menu-item-128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-128"><a href="https://gakken-idn.id" onclick="__gaTracker('send', 'event', 'outbound-widget', 'https://gakken-idn.id', 'Gakken P2KB');">Gakken P2KB</a></li>
            <li id="menu-item-129" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-129"><a href="http://gakken-idn.co.id">PT. Gakken Health and Education Indonesia</a></li>
          </ul></div></div><div class="col-md-4 col-sm-3 widget"><h3>Newsletter</a> </h3>			<div class="textwidget"><!-- Begin MailChimp Signup Form -->
            <style type="text/css">
            #mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif;}
            #mc_embed_signup form{padding: 0;}
            </style>
            <div id="mc_embed_signup">
            <form action="//gakken-idn.us13.list-manage.com/subscribe/post?u=8fd0913ea46d91bec993867fd&amp;id=30fba8f95d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
            <div class="mc-field-group">
            <label for="mce-EMAIL">Email <span class="asterisk">*</span>
            </label>
            <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
            </div>
            <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
            </div>
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8fd0913ea46d91bec993867fd_30fba8f95d" tabindex="-1" value=""></div>
            <div class="clear"><button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary" style="background-color: #EC1F27;
            color: #fff;
            border-color: #D20811; margin-top:5px;">Subscribe</button></div>
            </div>
            </form>
            </div>
            <!--End mc_embed_signup--></div>
                </div>				</div>

                    <div class="small text-center" style="margin-top:45px;">
                      Copyright 2015 &copy; PT. Gakken Health and Education Indonesia. All Rights Reserved.
                    </div>
                  </div>
                </section>
    </footer>


    <script src="javascripts/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="javascripts/bootstrap-sprockets.js" crossorigin="anonymous"></script>
    <script src="javascripts/home.js" crossorigin="anonymous"></script>
  </body>
</html>
