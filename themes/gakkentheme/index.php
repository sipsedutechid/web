<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    <?php get_header(); ?>

    <?php include("slider.php");?>

    <div class="full-width container-fluid">

      <div class="main-container">

        <div class="row" style="margin-left: 0px; margin-right: 0px; ">

        <?php include("home-article.php");?>


        </div>

      </div>

    </div>
    <!-- </div> -->

    <?php get_footer();?>
