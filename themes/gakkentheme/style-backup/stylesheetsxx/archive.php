<?php get_header(); ?>


<div class="full-width container-fluid">

  <div class="main-container">

    <div class="article-wrapper col-sm-9">
      <div class="single-article-container">

        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

        <div class="page-header">
        <h1 class="title"> <?php the_title();?>  </h1>
        <div class="sub-desc"> <small> <?php the_date($post_ID); ?>  |  by <?php the_author_posts_link(); ?>. </small> </div>
      </div>


        <article>

          <center> <?php the_post_thumbnail('large'); ?> </center>

          <?php the_content();?>

        </article>

      <?php endwhile; endif;?>

      </div>
    </div>

    <?php get_sidebar();?>

  </div>

</div>
<!-- </div> -->

<?php get_footer();?>
