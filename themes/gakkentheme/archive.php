<?php
/*Template Name : All Posts */

?>

 <?php get_header();?>

    <div class="full-width container-fluid">

      <?php if(is_category() || is_author() ):
        get_template_part( 'slider', 'single' );


      endif;

      ;?>
      

      <div class="main-container" style="margin-top: 20px;">



        <h1 class="super-title">
          <?php
      		// Start the loop.

            if(is_tag()):
              echo "Tag : ";

            elseif(is_author()):
              echo "Author : ";

            elseif(is_category()):
              echo "Category : ";

            endif;

      		?>

          <?php wp_title('', true, 'right');?>
        </h1>

        <div class="row" style="margin-left: 0px; margin-right: 0px;">
          <?php
      		// Start the loop.

            if(is_tag()):
              get_template_part( 'content', 'tag' );

            elseif(is_author()):
              get_template_part('content', 'author');

            elseif(is_category()):
              get_template_part('content', 'category');

            endif;

          // the_posts_pagination( array(
    			// 	'prev_text'          => __( 'Previous page', 'gakkentheme' ),
    			// 	'next_text'          => __( 'Next page', 'gakkentheme' ),
    			// 	'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'gakkentheme' ) . ' </span>',
    			// ) );

      		?>


          <?php get_sidebar();?>

        </div>
          </div>
        </div>
      </div>

    <?php get_footer();?>
