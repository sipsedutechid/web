<?php
    /*Template Name : Obat page */

    get_header();
    // all obat
    if($wp_query->query_vars['drug'] == 'all'):
        $topic_account   = json_decode(do_shortcode('[gkaccount-subscribe-topics]'));
        $account = json_decode(do_shortcode('[gkaccount-login-status]'));
        ?>

    <div ng-app="drugsApp">

    	<div ng-controller="searchCtrl as searchcon" style="padding-top: 105px;">
			<div class="jumbotron" style="background:#A41E22;padding:15px 0;position: fixed;width:100%;top: 70px;z-index: 1033;">
			    <div class="full-width container-fluid">
			      	<div class="row">
			            <div class="main-container">
				            <div class="col-lg-6">
				            	<div class="search" style="margin-bottom:0;">
				                	<div class="input-group stylish-input-group">
					                	<input type="text" ng-model="searchcon.filters.term" class="form-control input-lg" placeholder="Cari obat berdasarkan nama, pabrik atau kategori" ng-keypress="searchcon.search($event)" />
					                	<span class="input-group-addon">
					                    	<button type="button" ng-click="searchcon.clear_search();" ng-if="searchcon.is_searched">
					                        	<span class="fa fa-remove"></span>
					                    	</button>
					                    	<button type="button" ng-click="searchcon.search();">
					                        	<span class="fa fa-search"></span>
					                    	</button>
					                	</span>
					                </div>
				                </div>
				            </div>
			            </div>
					</div>
			    </div>
			</div>

		    <!-- LIST CONTROLLER -->
		    <div ng-controller="listCtrl as listcon" ng-show="listcon.is_active">
				<div class="jumbotron" style="background:#fff;margin-top:-30px;" ng-show="searchcon.is_show_tutorial">
					<div class="full-width container-fluid">
						<div class="row">
							<div class="col-md-4 text-center">
								<img src="<?php echo get_template_directory_uri() ?>/images/drugs-tutorial-01.png" />
								<p>Ketik di kotak pencarian dan tekan Enter untuk mencari obat berdasarkan nama, pabrik atau kategori</p>
							</div>
							<div class="col-md-4 text-center">
								<img src="<?php echo get_template_directory_uri() ?>/images/drugs-tutorial-02.png" />
								<p>Klik nama pabrik atau kategori untuk menyaring hasil pencarian</p>
							</div>
							<div class="col-md-4 text-center">
								<img src="<?php echo get_template_directory_uri() ?>/images/drugs-tutorial-03.png" />
								<p>Klik nama obat untuk melihat rinciannya</p>
							</div>
						</div>
					</div>
				</div>

			    <div class="full-width container-fluid">
			        <div class="main-container">
			            <div class="article-wrapper col-lg-12">
			            	<div style="margin-bottom:30px;" ng-if="searchcon.filters.kategori.label || searchcon.filters.pabrik.label">
			            		<span class="label label-primary" style="font-size:16px;padding:10px;margin-right:5px;" ng-if="searchcon.filters.kategori.label">
			            			Kategori: {{ searchcon.filters.kategori.label }}&ensp;<a href="#" ng-click="searchcon.clear_filter('kategori')" style="color:#fff;"><b class="fa fa-remove fa-fw"></b></a>
			            		</span>
			            		<span class="label label-primary" style="font-size:16px;padding:10px;" ng-if="searchcon.filters.pabrik.label">
			            			Pabrik: {{ searchcon.filters.pabrik.label }}&ensp;<a href="#" ng-click="searchcon.clear_filter('pabrik')" style="color:#fff;"><b class="fa fa-remove fa-fw"></b></a>
			            		</span>
			            	</div>
				            <div infinite-scroll="listcon.next()" class="panel panel-default" ng-show="listcon.drugs.data.length" style="position: relative;">
				            	<table class="table table-hover">
				            		<thead>
				            			<tr>
				            				<th>Obat</th>
				            				<th>Pabrik</th>
				            				<th>Kategori</th>
				            			</tr>
				            		</thead>
				            		<tbody>
					            		<tr ng-repeat="drug in listcon.drugs.data">
					            			<td>
					            				<?php if ($account->status == 'authenticated'): ?>
						            				<?php if ($account->subscription != null): ?>
							            				<a href="#" ng-click="listcon.select_single($event, drug.slug, drug.name)">{{ drug.name }}</a>
							            			<?php else: ?>
							            				<a href="<?php bloginfo('url') ?>/berlangganan">{{ drug.name }}</a>
							            			<?php endif; ?>
						            			<?php else: ?>
						            				<a href="#" data-gakken-accounts="sign-in-btn" data-toggle="modal" data-target="#signin">{{ drug.name }}</a>
						            			<?php endif; ?>
						            		</td>
						            		<td>
						            			<a href="#" ng-click="$event.preventDefault();searchcon.filter({type: 'pabrik', value: drug.drug_manufacturer.slug, label: drug.drug_manufacturer.name})">{{ drug.drug_manufacturer.name }}</a>
						            		</td>
						            		<td>
					            				<a href="#" ng-click="$event.preventDefault();searchcon.filter({type: 'kategori', value: drug.drug_class.code, label: drug.drug_class.name})">{{ drug.drug_class.name }} <span ng-show="drug.drug_subclass">/ {{ drug.drug_subclass.name }}</span></a>
						            		</td>
					            		</tr>
					            	</tbody>
				            	</table>

				            	<div ng-if="listcon.is_loading" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;background: rgba(255,255,255,0.7);"></div>
			            	</div>

				            <div class="text-center" ng-show="listcon.is_loading" style="margin:45px 0;">
					            <b class="fa fa-circle-o-notch fa-spin fa-2x"></b>
					            <p style="margin-top:5px;">Memuat..</p>
				            </div>
				            <div class="text-center" ng-if="!listcon.drugs.data.length && !listcon.is_loading" style="margin:45px 0;">
					            <b class="fa fa-frown-o fa-5x"></b>
					            <p style="margin-top:5px;">Obat yang Anda cari tidak dapat ditemukan</p>
				            </div>
			        	</div>
			        </div>
			    </div>
		    </div>

		    <!-- SINGLE DRUG SECTION -->
		    <div ng-controller="singleCtrl as singlecon" ng-show="singlecon.is_active">
		    	<div class="full-width container-fluid">
		        	<div class="drugs" style="padding-top:15px; margin-bottom: 20px;">

		          		<aside class="drugs-aside col-sm-3">

		          			<button type="button" ng-click="singlecon.back();" class="btn btn-block btn-lg">
		          				<b class="fa fa-arrow-left pull-left"></b> Kembali
		          			</button>
		          			<br />

		            		<div class="panel panel-default" ng-if="singlecon.more_drugs.length">
	            				<div class="panel-heading"><div class="panel-title h4">Semua Obat {{ singlecon.drug.drug_manufacturer.name }}</div></div>
		              			<div class="list-group">
					                <a href="{{ drug.slug }}" class="list-group-item" ng-repeat="drug in singlecon.more_drugs track by $index" ng-show="$index < singlecon.more_drugs_limit">
					                	{{ drug.name }}
					                	<div class="small text-muted" ng-bind="drug.drug_class.name"></div>
					                </a>
					                <a href="#" ng-click="$event.preventDefault();singlecon.more_drugs_limit = singlecon.more_drugs.length" class="list-group-item text-center text-uppercase" ng-if="singlecon.more_drugs_limit < singlecon.more_drugs.length">Lihat semua</a>
		              			</div>
		            		</div>
		          		</aside>

		          		<div class="drugs-container col-sm-9" style="min-height:600px;">
	        				<?php if ($account->subscription): ?>
			            		<div class="drugs-top-container">
			              			<div class="row">
			              				<div class="img col-sm-3" ng-if="singlecon.drug.drug_manufacturer.logo_image.length" style="height: 60px;">
			                				<img ng-src="<?php echo do_shortcode('[gkvault-get-base-url]'); ?>drug/manufacturer/logo/{{ singlecon.drug.drug_manufacturer.slug }}" style="width:auto !important; height: 100% !important;"/>
			              				</div>

				              				<div class="img col-sm-1" style="padding-top: 15px; height: 100px; position: absolute; width: 100px; right: 10px; top: 0px;" ng-if="singlecon.drug.classification.length">
				                				<img ng-src="<?php echo get_template_directory_uri() ?>/images/{{ singlecon.dist_images[singlecon.drug.classification] }}" />
				              				</div>

			              				<div class="descriptions col-sm-12" style="margin: 20px;">
			                				<h1 ng-bind="singlecon.drug.name"></h1>
			                				<h4>
			                					<small>
			                						{{ singlecon.drug.drug_manufacturer.name }}
			              							<span ng-if="singlecon.drug.drug_marketer">(dipasarkan oleh {{ singlecon.drug.drug_marketer.name }})</span>
			            						</small>
			            					</h4>
			              				</div>
			              				
			              			</div>
			            		</div>

			            		<div class="drugs-middle-container">
			              			<div class="extra">
			                			<div class="col-sm-3">
			                  				Kelas
			                			</div>
			                			<div class="col-sm-8">
			                  				<p>
			                  					{{ singlecon.drug.drug_class.name }}
			                  					<span ng-if="singlecon.drug.drug_subclass">/ {{ singlecon.drug.drug_subclass.name }}</span>
			              					</p>
			                			</div>
			              			</div>
			              			<div class="extra" ng-if="singlecon.drug.indication">
			                			<div class="col-sm-3">
			                  				Indikasi
			                			</div>
			                			<div class="col-sm-8">
			                  				<p ng-bind="singlecon.drug.indication"></p>
			                			</div>
			              			</div>
			              			<div class="extra" ng-if="singlecon.drug.contraindication">
			                			<div class="col-sm-3">
			                  				Kontra Indikasi
			                			</div>
			                			<div class="col-sm-8">
			                  				<p ng-bind="singlecon.drug.contraindication"></p>
			                			</div>
			              			</div>
			              			<div class="extra" ng-if="singlecon.drug.precautions">
			                			<div class="col-sm-3">
			                 				Perhatian Khusus
			                			</div>
			                			<div class="col-sm-8">
			                  				<p ng-bind="singlecon.drug.precautions"></p>
			                			</div>
			              			</div>
			              			<div class="extra" ng-if="singlecon.drug.adverse_reactions">
			                			<div class="col-sm-3">
			                  				Efek Samping
			                			</div>
			                			<div class="col-sm-8">
			                  				<p ng-bind="singlecon.drug.adverse_reactions"></p>
			                			</div>
			              			</div>
			              			<div class="extra" ng-if="singlecon.drug.interactions">
				            			<div class="col-sm-3">
				              				Interaksi
				            			</div>
				            			<div class="col-sm-8">
				              				<p ng-bind="singlecon.drug.interactions"></p>
				            			</div>
				          			</div>
			            			<?php if (false): //in_array($drug->administration, ['cc', 'scc', 's/cc'])): ?>
				              			<div class="extra">
				                			<div class="col-sm-3">
				                  				Administrasi
				                			</div>
				                			<div class="col-sm-8">
				                  				<p>
				                  					<?php echo $administration_labels[$drug->administration]; ?>
				                  					<?php echo $drug->administration_extra; ?>
			                  					</p>
				                			</div>
				              			</div>
				              		<?php endif; ?>
			            			<?php if (false): //in_array($drug->pregnancy_category, ['a', 'b', 'c', 'd', 'x'])): ?>
				              			<div class="extra">
				                			<div class="col-sm-3">
				                  				Keamanan Kehamilan
				                			</div>
				                			<div class="col-sm-8">
				                  				<p><?php echo strtoupper($drug->pregnancy_category); ?></p>
				                			</div>
				              			</div>
				              		<?php endif; ?>

			              			<div class="extra" ng-if="singlecon.drug.dosages.length">
				              			<h2>Dosis</h2>
			                			<div class="forms" style="padding:0;">
				                  			<table class="table">
				                    			<thead>
				                      				<tr>
								                        <th>Jenis</th>
								                        <th>Sediaan</th>
								                        <th>Keterangan</th>
				                      				</tr>
				                    			</thead>
				                    			<tbody>
				                      				<tr ng-repeat="dosage in singlecon.drug.dosages">
								                        <td ng-bind-html="dosage.name"></td>
								                        <td>
								                        	{{ dosage.drug_form == null ? "Semua" : dosage.drug_form.name }}
								                        	{{ dosage.administration ? "(" + dosage.administration + ")" : "" }}
							                        	</td>
								                        <td ng-bind-html="dosage.description"></td>
				                      				</tr>
				                    			</tbody>
				                  			</table>
				                		</div>
				                	</div>

				                	<div class="extra list-pack" ng-if="singlecon.drug.generics.length">
				                  		<h2>Komposisi</h2>
			                			<div class="forms" style="padding:0;">
		  		                  			<table class="table">
		  		                    			<thead>
		  		                      				<tr>
		  						                        <th>Sediaan</th>
		  						                        <th>Komposisi</th>
		  		                      				</tr>
		  		                    			</thead>
		  		                    			<tbody>
		  		                      				<tr ng-repeat="generic in singlecon.drug.generics">
		  						                        <td>{{ generic.drug_form.name ? generic.drug_form.name : "Semua" }}</td>
		  						                        <td>{{ generic.generic.name }} <span ng-if="generic.quantity">- {{ generic.quantity }} {{ generic.unit }}</span></td>
		  		                      				</tr>
		  		                    			</tbody>
		  		                  			</table>
					            		</div>
				            		</div>

					                <div class="extra list-pack" ng-if="singlecon.drug.packagings.length">
				                  		<h2>Kemasan &amp; Harga</h2>
				              			<ul>
						                    <li ng-repeat="packaging in singlecon.drug.packagings">
						                    	<span ng-bind-html="packaging.drug_form.name"></span>
						                    	<span ng-bind-html="packaging.name"></span>
						                    	<span ng-if="packaging.quantity">
						                    		({{ packaging.quantity + (packaging.quantity_2 > 0 ? " x " + packaging.quantity_2 + (packaging.quantity_3 > 0 ? " x " + packaging.quantity_3 : "") : "") }})
						                    	</span>
						                    	<span ng-if="packaging.price > 0">@ Rp <span ng-bind="packaging.price_label"></span></span>
						                    </li>
				              			</ul>
				            		</div>

				              	</div>
							<?php elseif($account->status == 'authenticated'): ?>
								<p class="text-center" style="margin-top: 45px;">
						            <b class="fa fa-frown-o fa-5x"></b>
						            <br />
									Untuk melihat rincian obat ini, silahkan <a href="<?php echo get_option('gakken_accounts_base_url', ''); ?>subscribe" style="color:#a4151a;">berlangganan di layanan kami</a> terlebih dahulu.
								</p>
							<?php else: ?>

								<p class="text-center" style="margin-top: 45px;">
						            <b class="fa fa-frown-o fa-5x"></b>
						            <br />
									Untuk melihat rincian obat ini, harap <a href="#" data-gakken-accounts="sign-in-btn" data-toggle="modal" data-target="#signin" style="color:#a4151a;">masuk</a> terlebih dahulu.
								</p>

							<?php endif; ?>
			            </div>
			        </div>
			    </div>
			</div>

	    </div>
    </div>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/loading-bar.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/ng-infinite-scroll.min.js"></script>
    <script type="text/javascript">
	    var api_base_url = "<?php echo do_shortcode('[gkvault-get-base-url]') ?>";
    </script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-sanitize.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/javascripts/drugs.js"></script>
	<?php
	endif;
get_footer();
?>
