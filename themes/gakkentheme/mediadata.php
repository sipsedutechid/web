<?php
    /*Template Name : Obat page */

    get_header();
    // all obat
    $vault_url          = do_shortcode('[gkvault-get-base-url]');

    if($wp_query->query_vars['media'] == 'all'):
        $topic_account   = json_decode(do_shortcode('[gkaccount-subscribe-topics]'));
        $account = json_decode(do_shortcode('[gkaccount-login-status]'));
        ?>
        <div ng-app='mnediadataApps'>
            <div
                class="full-container media-intro"
                style="
                    background: url('<?= get_template_directory_uri() . '/images/mediadata.jpg' ?>') no-repeat center center fixed;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover; ">
                <div class="media-title">
                    <span class="title">Media & Data</span> <br>
                    <span class="subtitle">Powered By Gakken</span>
                </div>
            </div>
            <!-- <div class="featured-media full-width">
                <div class="wrap row">
                    <?php for($i = 1; $i <= 6; $i++): ?>
                        <div class="col-sm-4 thumb">
                            <img src="http://vault.gakken-idn.id/journal/files/image/1.png" alt="" />
                        </div>
                    <?php endfor; ?>
                </div>

            </div> -->
            <div class="media-content full-width" ng-controller='mediadataCtrl as mediadatacon' style="margin-top:20px;">
                <div class="media-tollbar">
                    <div class="media-searching">
                        <input type="text" ng-model="mediadata.keyword" ng-keyup="mediadatacon.search($event, 'searchbox')" class="media-field-search" placeholder="Cari di sini">
                        <button type="button" ng-click="mediadatacon.removeKeyword()" name="bnt_remove_keyword" class="btn-transparant" ng-show="mediadata.keyword.length > 0" style="position: absolute; right: 25px;   top: 6px;">
                            <span class="fa fa-remove"></span>
                        </button>
                        <button type="button" ng-click="mediadatacon.search($event, 'btnsearch')" name="button" class="media-btn-search">
                            <i class="fa fa-search fa-lg"></i>
                        </button>
                    </div>
                    <div class="media-category">
                        <select ng-model="mediadata.appearance" ng-change="mediadatacon.search($event, 'category')">
                            <option value="#">Semua</option>
                            <option value="text">Teks</option>
                            <option value="image">Gambar</option>
                            <option value="video">Video</option>
                        </select>
                    </div>
                    <div class="media-sorting">
                        Sort By:
                        <select ng-model="mediadata.sortby" ng-change="mediadatacon.search($event, 'sortby')">
                            <option value="newest">Terbaru</option>
                            <option value="oldest">Terlama</option>
                            <option value="abjadasc">A - Z</option>
                            <option value="abjaddesc">Z - A</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-top:60px" infinite-scroll="mediadatacon.next()">
                        <div class="media-thumb col-sm-2" ng-repeat="mediadata in mediadatacon.mediadatas.data">
                            <div class="image-cropper" ng-switch on="mediadata.appearance" data-toggle="modal" data-target="#singleMediaData" ng-click="mediadatacon.callSingle(mediadata.slug)">
                                <!-- <img src="http://www.electricvelocity.com.au/Upload/Blogs/smart-e-bike-side_2.jpg" class="rounded" /> -->

                                <img ng-src="<?= $vault_url ?>content/files/{{ mediadata.data }}" alt="{{ mediadata.title }}" ng-if="mediadata.type == 'file' && mediadata.appearance =='image'" class="rounded" />
                                <i class="fa fa-video-camera" ng-switch-when="video" style="background: #90e4ff;"></i>
                                <i class="fa fa-line-chart" ng-switch-when="image" style="background: #ff9090;"></i>
                                <i class="fa fa-info" ng-switch-when="text" style="background: #ffca90;"></i>
                            </div>
                            <div class="title">
                                <a data-toggle="modal" data-target="#singleMediaData" ng-click="mediadatacon.callSingle(mediadata.slug)" style="cursor:pointer;"> {{ mediadata.title }}</a>
                            </div>
                        </div>
                </div>
            </div>

            <!-- modal single content section -->
            <div class="modal fade" id="singleMediaData" tabindex="-1" role="dialog" aria-labelledby="singleMediaDataLabel" ng-controller="mediadatasingleCtrl as mediadatasinglecon" ng-show="mediadatasinglecon.show">
                <div class="modal-dialog" role="document" style="width: auto; max-width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="singleMediaDataLabel">{{ mediadatasinglecon.mediadata.title }}</h4>
                        </div>
                        <div class="modal-body" ng-if="mediadatasinglecon.mediadata.type == 'text'" ng-switch on="mediadatasinglecon.mediadata.appearance">
                            <div ng-switch-when="text" ng-bind-html="trust(mediadatasinglecon.mediadata.data)"></div>
                        </div>
                        <div class="modal-body" ng-if="mediadatasinglecon.mediadata.type == 'embed'" ng-switch on="mediadatasinglecon.mediadata.appearance">
                            <iframe ng-src="{{ mediadatasinglecon.mediadata.data }}" width="854px" height="480px"></iframe>
                        </div>
                        <div class="modal-body" ng-if="mediadatasinglecon.mediadata.type == 'file'" ng-switch on="mediadatasinglecon.mediadata.appearance" style="height:600px; overflow-y:auto;">
                            <div ng-switch-when="text">
                                <object data="<?= do_shortcode('[gkvault-get-base-url]'); ?>content/files/{{ mediadatasinglecon.mediadata.data }}" style="width:100%;min-height:768px;"></object>
                            </div>
                            <div ng-switch-when="image">
                                <img ng-src="<?= $vault_url ?>content/files/{{ mediadatasinglecon.mediadata.data }}" alt="{{ mediadatasinglecon.mediadata.title }}" style="max-width: 100%;"/>
                            </div>
                            <div ng-switch-when="video">
                                <video ng-src="<?= $vault_url ?>content/files/{{ mediadatasinglecon.mediadata.data }}"></video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- endmodal -->
        </div>

        <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/angular.min.js"></script>
        <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/loading-bar.js"></script>
        <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/ng-infinite-scroll.min.js"></script>
        <script type="text/javascript">
            var url_mediadata       = "<?= $vault_url . 'api/content/list' ?>";
            var url_mediadatasingle = "<?= $vault_url . 'api/content' ?>";
        </script>
        <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/javascripts/mediadata.js"></script>
    <?php endif; ?>
        <?php get_footer(); ?>
