<?php

// Register options
add_action('admin_init', 'gakken_accounts_settings');
function gakken_accounts_settings()
{
	add_settings_section(
		"gakken_accounts_settings_general", // id
		"General Settings", 				// title
		null, 								// callback
		"gakken_accounts"					// page
		);
	add_settings_field(
		"gakken_accounts_base_url", 		// id
		"Base URL", 						// title
		"gakken_accounts_base_url_field", 	// callback
		"gakken_accounts", 					// page
		"gakken_accounts_settings_general"	// section
		);
    
	register_setting(
		'gakken_accounts', 					// settings section
		'gakken_accounts_base_url'			// option name
		);
}

// Admin menu
add_action('admin_menu', 'gakken_accounts_admin_menu');
function gakken_accounts_admin_menu()
{
	add_menu_page("Gakken Accounts", "Gakken Accounts", "manage_options", "gakken_accounts", "gakken_accounts_settings_page", null, 99);
}

// Settings page
function gakken_accounts_settings_page()
{
	?>
	<div class="wrap">
		<h1>Gakken Accounts</h1>
		<form method="post" action="options.php">
			<?php 
			settings_fields('gakken_accounts');
			do_settings_sections('gakken_accounts');
			submit_button();
			?>
		</form>
	</div>
	<?php
}

// Fields
function gakken_accounts_base_url_field()
{
	?><input type="text" name="gakken_accounts_base_url" id="gakken_accounts_base_url" value="<?php echo get_option('gakken_accounts_base_url'); ?>" style="min-width:300px;" /><?php
}
