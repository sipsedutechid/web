var listTopic       = document.getElementById('topic-list') ,
    loadMoreBtn     = document.getElementById('load-more-btn') ,
    loadingContent  = document.getElementById('loading-content');

var lastId = 0;

var script      = document.getElementById("topic_script") ,
    srcArr      = script.getAttribute("src").split("?") ,
    arguments   = srcArr[1].split("&");

var arg = [];

arguments.forEach( function(argument) {
    var argKV = argument.split('=');

    arg[ argKV[0] ] = String(argKV[1])
        .replace('&amp;', /&/g)
        .replace('&lt;', /</g)
        .replace('&gt;', />/g)
        .replace('&quot;', /"/g);
});

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

var getContent = function() {
    var xmlhttp = new XMLHttpRequest();

    var formData  = new FormData() ,
        myHeaders = new Headers();

    formData.append('last_id', lastId);
    console.log(getCookie('gksession'));

    var myInit = {
        method  : 'post' ,
        headers : myHeaders ,
        body    : formData
    };

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse( xmlhttp.responseText );

            lastId = (data.last_id > 0) ? data.last_id : 0;
            renderTopic( data.topics );

            if (lastId === 0)
                loadMoreBtn.classList.add('hidden');
            else
                loadMoreBtn.classList.remove('hidden');

            loadMoreBtn.removeAttribute('disabled');
            loadingContent.classList.add('hidden');
       }
    };
    xmlhttp.open("POST", arg.url + "/api/topics/list/recent", true);
    xmlhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xmlhttp.send(formData);

    loadingContent.classList.remove('hidden');
    loadMoreBtn.setAttribute('disabled', 'true');
}

var renderTopic = function( data ) {
    var html ,
        http    = location.protocol ,
        slashes = http.concat("//") ,
        host    = slashes.concat(window.location.hostname);

    data.forEach( function(topic) {
        html  = '<div class="all-article-wrapper"> <div class="row"><div class="container col-sm-12">';
        html += '<h2> <a href="' + host + '/topik/judul/' + topic.slug + '/"> ' + topic.title + ' </h2> </a>';
        html += '<div class="sub-desc"> ' + topic.created_at + ' | <b>' + topic.level.name + '</b> | By  </div>';
        html += topic.meta_desc;
        html += '</div></div></div>';

        listTopic.insertAdjacentHTML('beforeend', html);
    });
}

getContent();

loadMoreBtn.onclick = getContent;
