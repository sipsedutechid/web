<?php

/*
Plugin Name: Gakken Vault
Description: WP plugin to the Gakken Vault app
Author: Gakken Indonesia IT Team
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

require_once('settings.php');

add_action('init', 'gakken_vault_register_shortcodes');

function gakken_vault_register_shortcodes()
{
	add_shortcode('gkvault-drug-classes-list', 'gakken_vault_drugs_classes_shortcode');
	add_shortcode('gkvault-drugs-search', 'gakken_vault_drugs_search_shortcode');
	add_shortcode('gkvault-drugs-single', 'gakken_vault_drugs_single_shortcode');
	// add_shortcode('gkvault-topics-list', 'gakken_vault_topics_append_scripts');
	add_shortcode('gkvault-topics-single', 'gakken_vault_topics_single_shortcode');
	// add_shortcode('gkvault-topics-list-by-option', 'gakken_vault_list_topics_by_option');
	add_shortcode('gkvault-topics-latest', 'gakken_vault_topics_latest');
	add_shortcode('gkvault-disease-list', 'gakken_vault_list_disease_shortcode');
	add_shortcode('gkvault-journals-list', 'gakken_vault_list_journals_shortcode');
	add_shortcode('gkvault-journals-single', 'gakken_vault_single_journals_shortcode');
	add_shortcode('gkvault-journals-article', 'gakken_vault_journals_article_shortcode');
	add_shortcode('gkvault-journals-latest', 'gakken_vault_latest_journals_shortcode');
	add_shortcode('gkvault-disease-topics-list', 'gakken_vault_list_topic_disease_shortcode');
	add_shortcode('gkvault-contents-single', 'gakken_vault_single_contents_shortcode');
	add_shortcode('gkvault-get-base-url', 'gakken_vault_get_base_url');
	add_shortcode('gkvault-get-journal-category', 'gakken_vault_journal_category_shortcode');
	add_shortcode('gkaccount-login-status', 'gakken_account_login_status');
	add_shortcode('gkaccount-subscribe-topics', 'gakken_account_subscribe_topics');
}

// Shortcodes
function gakken_vault_drugs_classes_shortcode()
{

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/drugs/classes/short');
    if (!is_array($response))
    	return var_dump($response);
    return $response['body'];
    // return json_decode($response['body']);
}
function gakken_vault_drugs_search_shortcode($atts)
{
	$drugs_list_atts = shortcode_atts([
        'limit' 	=> 15,
        'title'		=> 'Hasil Pencarian',
        'term'		=> ''
    ], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/drugs/search?q=' . $drugs_list_atts['term'] . '&limit=' . $drugs_list_atts['limit']);
    if (!is_array($response))
    	return var_dump($response);
    $drugs = json_decode($response['body']);

    if (count($drugs)) {
		?>
		<h1><?php echo $drugs_list_atts['title'] ?></h1>
    	<div class="list-group">
		<?php
    	foreach ($drugs as $index => $drug) {
    		$linkUrl = get_bloginfo('url') . '/obat/' . $drug->slug;
	    	?>
    	 	<a href="<?php echo $linkUrl ?>" class="list-group-item">
	    	 	<p style="font-size:20px;margin-bottom:5px;"><?php echo $drug->name; ?></p>
    	    	<span class="description">
    	    		<?php echo $drug->drug_manufacturer->name; ?><br />
    	    		<?php echo $drug->drug_subclass->name; ?> / <?php echo $drug->drug_class->name; ?>
	    		</span>
    	  	</a>
	    	<?php
	    }
    	?></div><?php
    } else {
    	echo "<p>Maaf, kami tidak menemukan obat yang Anda cari.</p>";
    }
}
function gakken_vault_drugs_single_shortcode($atts)
{
	$drugs_list_atts = shortcode_atts(['drug' => ''], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/drug/' . $drugs_list_atts['drug']);
    if (!is_array($response))
    	return var_dump($response);
    return $response['body'];
}

function gakken_vault_topics_append_scripts()
{
	$scripts = [
		["id" => "topic_script" , "url" =>'topic.js?url=' . htmlentities(gakken_vault_get_base_url())]
	];
	foreach ($scripts as $script) {
		?><script id="<?= $script['id'] ?>" type="text/javascript" src="<?= bloginfo('url'); ?>/wp-content/plugins/gakken-vault/js/<?= $script['url'] ?>"></script><?php
	}
}

function gakken_vault_list_topics_by_option($atts)
{
	$a = shortcode_atts( array(
	   'category' => 'recent'
   	), $atts );

    $response = wp_remote_post(gakken_vault_get_base_url() . 'api/topics/list/' . $a['category'] );

	return $response['body'];
}

function gakken_vault_topics_single_shortcode( $atts )
{
	$a = shortcode_atts( array(
	   'slug' => 'null'
   	), $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/topic/' . $a['slug'] );

	return $response['body'];
}

function gakken_vault_list_disease_shortcode($atts)
{
	$a = shortcode_atts([
        'slug' 	=> null
    ], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/disease/list/' . $a['slug'] ?: '');

	return $response['body'];
}

function gakken_vault_list_topic_disease_shortcode($atts)
{
	$a = shortcode_atts([
        'disease' 	=> 'null'
    ], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/' . $a['disease'] . '/topic/list');

	return $response['body'];
}

function gakken_vault_topics_latest()
{
    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/topic/latest' );

	return $response['body'];
}

function gakken_vault_get_base_url()
{
	$baseUrl = get_option('gakken_vault_base_url', '');
	if (substr($baseUrl, -1) != '/') // check for trailing slash
		$baseUrl = $baseUrl . '/';

	return $baseUrl;
}

function gakken_account_get_base_url()
{
	$baseUrl = get_option('gakken_accounts_base_url', '');
	if (substr($baseUrl, -1) != '/') // check for trailing slash
		$baseUrl = $baseUrl . '/';

	return $baseUrl;
}

function gakken_vault_single_journals_shortcode($atts)
{
	$a = shortcode_atts([
        'slug' 	=> null
    ], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/journal/' . $a['slug'] ?: '');

	return $response['body'];
}

function gakken_vault_list_journals_shortcode($atts)
{
	$a = shortcode_atts([
        'page' 	=> 1 ,
		'limit'=> 20
    ], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/journal/list/' . $a['page'] . '/' . $a['limit']);

	return $response['body'];
}

function gakken_vault_latest_journals_shortcode($atts) {
	$a = shortcode_atts([
		'limit'=> 8
    ], $atts );

	$response = wp_remote_get(gakken_vault_get_base_url() . 'api/journal/latest/' . $a['limit']);

	return $response['body'];
}

function gakken_vault_single_contents_shortcode($atts)
{
	$a = shortcode_atts([
        'slug' 	=> null
    ], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/content/' . $a['slug'] ?: '');

	return $response['body'];
}

function gakken_vault_journal_category_shortcode()
{
    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/journal/category/');

	return $response['body'];
}

function gakken_account_login_status()
{
	$session = ['gksession' => (isset($_COOKIE['gksession'])) ? $_COOKIE['gksession'] : ''];
    $response = wp_remote_get(gakken_account_get_base_url() . 'api/status/', ['cookies' => $session] );

	return $response['body'];
}

function gakken_vault_journals_article_shortcode($atts)
{
	$a = shortcode_atts([
        'slug' 	=> null
    ], $atts );

    $response = wp_remote_get(gakken_vault_get_base_url() . 'api/journal/article/' . $a['slug'] ?: '');

	return $response['body'];
}

function gakken_account_subscribe_topics($atts)
{
	$session = isset($_COOKIE['gksession']) ? ['gksession' => $_COOKIE['gksession']] : [];
    $response = wp_remote_get(gakken_account_get_base_url() . 'api/available-topics', ['cookies' => $session]);

	return $response['body'];
}
