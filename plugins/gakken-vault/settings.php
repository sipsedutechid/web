<?php

// Register options
add_action('admin_init', 'gakken_vault_settings');
function gakken_vault_settings()
{
	add_settings_section(
		"gakken_vault_settings_general", // id
		"General Settings", 			// title
		null, 							// callback
		"gakken_vault"					// page
		);
	add_settings_field(
		"gakken_vault_base_url", 		// id
		"Base URL", 					// title
		"gakken_vault_base_url_field", 	// callback
		"gakken_vault", 				// page
		"gakken_vault_settings_general"	// section
		);
    
	register_setting(
		'gakken_vault', 				// settings section
		'gakken_vault_base_url'			// option name
		);
}

// Admin menu
add_action('admin_menu', 'gakken_vault_admin_menu');
function gakken_vault_admin_menu()
{
	add_menu_page("Gakken VAULT", "Gakken VAULT", "manage_options", "gakken_vault", "gakken_vault_settings_page", null, 99);
}

// Settings page
function gakken_vault_settings_page()
{
	?>
	<div class="wrap">
		<h1>Gakken VAULT</h1>
		<form method="post" action="options.php">
			<?php 
			settings_fields('gakken_vault');
			do_settings_sections('gakken_vault');
			submit_button();
			?>
		</form>
	</div>
	<?php
}

// Fields
function gakken_vault_base_url_field()
{
	?><input type="text" name="gakken_vault_base_url" id="gakken_vault_base_url" value="<?php echo get_option('gakken_vault_base_url'); ?>" style="min-width:300px;" /><?php
}
